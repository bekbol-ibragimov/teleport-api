﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using DataAccess.Interfaces;
using Entities.Enums;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Utils;
using Web.UseCases.Auth.User.Dto;
using Web.UseCases.Base;
using WebApp.Interfaces.Services;

namespace Web.UseCases.Auth.User.Commands
{
    public class UserUpdateCommand: IRequest<bool>
    {
        public Guid Id { get; set; }
        public UserUpdateDto Dto { get; set; }
        
        
        public class Handler: BaseHandler<UserUpdateCommand, bool>
        {
            public Handler(IMapper mapper, IDbContext dbContext, IHttpContextService httpContextService) : base(mapper, dbContext, httpContextService)
            {
            }

            public override async Task<bool> Handle(UserUpdateCommand request, CancellationToken cancellationToken)
            {
                var user = await _dbContext.AuthUsers.FirstOrDefaultAsync(x => x.IsDeleted == false && x.Id == request.Id,
                        cancellationToken);
                if (user == null)
                {
                    throw ApiException.NotFound("User not found");
                }
                user.Iin = request.Dto.Iin;
                user.UserName = request.Dto.UserName;
                user.LastName = request.Dto.LastName;
                user.FirstName = request.Dto.FirstName;
                user.MiddleName = request.Dto.MiddleName;
                user.IsEmployeePool = request.Dto.IsEmployeePool;
                user.Pool = request.Dto.Pool;
                user.CompanyId = request.Dto.CompanyId;
                user.Sex = request.Dto.Sex;
                user.Email = request.Dto.Email;
                _dbContext.AuthUsers.Update(user);
                await _dbContext.SaveChangesAsync(cancellationToken);
                return true;
            }
        }
    }
}