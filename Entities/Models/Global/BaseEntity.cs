﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Entities.Models.Global
{
    public abstract class BaseEntity
    {
        [Key]
        public Guid Id { get; set; }

        /// <summary>
        /// Удален
        /// </summary>
        public bool IsDeleted { get; set; } = false;

        /// <summary>
        /// Дата создания сущности
        /// </summary>
        public DateTime CreatedAt { get; set; }

        /// <summary>
        /// Дата изменения сущности
        /// </summary>
        public DateTime UpdatedAt { get; set; }

        /// <summary>
        /// ID пользователя создавшего
        /// </summary>
        public Guid CreatedUserId { get; set; }

        /// <summary>
        /// ID пользователя изменившего
        /// </summary>
        public Guid UpdatedUserId { get; set; }
    }
}