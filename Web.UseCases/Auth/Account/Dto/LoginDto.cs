﻿using System.ComponentModel.DataAnnotations;

namespace Web.UseCases.Auth.Account.Dto
{
    public class LoginDto
    {
        /// <summary>
        /// Логин
        /// </summary>
        [Required(ErrorMessage = "Поле \"Email\" обязательно для заполнения")]
        public string UserName { get; set; }

        /// <summary>
        /// Пароль
        /// </summary>
        [Required(ErrorMessage = "Поле \"Пароль\" обязательно для заполнения")]
        public string Password { get; set; }
    }
}