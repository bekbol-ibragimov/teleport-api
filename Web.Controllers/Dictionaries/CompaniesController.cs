﻿using MediatR;
using Web.UseCases.Dictionaries.Companies.Commands;
using Web.UseCases.Dictionaries.Companies.Dto;
using Web.UseCases.Dictionaries.Companies.Queries;

namespace Web.Controllers.Dictionaries
{
    public class CompaniesController: BaseDictionaryController<CompanyIdto, CompanyOdto,
    CompanyGetListQuery, CompanyGetByIdQuery, CompanyCheckExistenceCodeQuery,
    CompanyCreateCommand, CompanyUpdateCommand, CompanyDeleteCommand>
    {
        public CompaniesController(ISender sender): base(sender)
        {
        }
    }
}