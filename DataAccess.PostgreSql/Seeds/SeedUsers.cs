﻿using System;
using Entities.Models.Auth;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.PostgreSql.Seeds
{
    public static class SeedUsers
    {
        public static void AddDefaultUsers(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().HasData(
                new User()
                {
                    Id = Guid.Parse("b13ba310-4f4e-4984-9ce9-7879d767d5b4"),
                    Email = "sa@demo.kz",
                    UserName = "sa",
                    LastName = "Саов",
                    FirstName = "Са",
                    MiddleName = "Саович",
                    PasswordHash =
                        "1e1bd7b4a2d36a821011acfa43aa7625e18ad0a76d5193b70cf43d1f6753b4da53c0b015a63d4d0054cfb0cfc38008a3bcda476af402b55ba10b3bf3667dac2a",
                });
            
            modelBuilder.Entity<UserPermission>().HasData(
                new UserPermission()
                {
                    Id = Guid.Parse("5aee0fa6-0ee8-4327-a821-f98d5245792e"),
                    UserId = Guid.Parse("b13ba310-4f4e-4984-9ce9-7879d767d5b4"),
                    PermissionId = Guid.Parse("3e3e1deb-8411-442a-8a54-3975a3de9804"),
            
                });
            
        }
    }
}