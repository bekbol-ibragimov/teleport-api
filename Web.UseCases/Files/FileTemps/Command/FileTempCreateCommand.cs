﻿using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using ApplicationService.Interfaces.Enums;
using AutoMapper;
using DataAccess.Interfaces;
using Entities.Models.Files;
using MediatR;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Web.UseCases.Base;
using Web.UseCases.Files.FileTemps.Dto;
using WebApp.Interfaces.Services;

namespace Web.UseCases.Files.FileTemps.Command
{
    public class FileTempCreateCommand : IRequest<FileTempOdto>
    {
        public IFormFile File { get; set; }

        public class Handler : BaseHandler<FileTempCreateCommand, FileTempOdto>
        {
            private readonly string _rootPath;

            public Handler(IMapper mapper, IDbContext dbContext, IHttpContextService httpContextService, IWebHostEnvironment environment) 
                : base(mapper, dbContext, httpContextService)
            {
                _rootPath = environment.WebRootPath;
            }

            public override async Task<FileTempOdto> Handle(FileTempCreateCommand request, CancellationToken cancellationToken)
            {
                var guid = Guid.NewGuid();
                var extension = Path.GetExtension(request.File.FileName);
                var newFilename = $"{guid.ToString()}{extension}";
                var fileTemp = new FileTemp()
                {
                    Extension = extension,
                    FileName = newFilename,
                    FullPath = $"/{FilePath.Files.ToString()}/{FilePath.TempFiles.ToString()}/{newFilename}",
                    OriginalFileName = request.File.FileName
                };
                var newFilePath = $"{_rootPath}{fileTemp.FullPath}";
                ValidatePath(newFilePath);
                await using (var stream = new FileStream(newFilePath, FileMode.CreateNew))
                {
                    await request.File.CopyToAsync(stream, cancellationToken);
                }
                await _dbContext.FileTemps.AddAsync(fileTemp, cancellationToken);
                await _dbContext.SaveChangesAsync(cancellationToken);
                return new FileTempOdto(){Id = fileTemp.Id, Path = fileTemp.FullPath};
                ;
            }
            
            private void ValidatePath(string filePath)
            {
                var dirPath = Path.GetDirectoryName(filePath);
                if (!Directory.Exists(dirPath))
                {
                    Directory.CreateDirectory(dirPath);
                }
            }
        }
    }
}