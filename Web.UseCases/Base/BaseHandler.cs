﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using DataAccess.Interfaces;
using Entities.Models.Global;
using MediatR;
using Microsoft.EntityFrameworkCore;
using WebApp.Interfaces.Services;

namespace Web.UseCases.Base
{
    public abstract class BaseHandler<TRequest, TResponse> : IRequestHandler<TRequest, TResponse>
        where TRequest : IRequest<TResponse>
    {
        protected readonly IMapper _mapper;
        protected readonly IDbContext _dbContext;
        protected readonly IHttpContextService _httpContextService;

        protected BaseHandler(IMapper mapper, IDbContext dbContext, IHttpContextService httpContextService)
        {
            _mapper = mapper;
            _dbContext = dbContext;
            _httpContextService = httpContextService;
        }

        public abstract Task<TResponse> Handle(TRequest request, CancellationToken cancellationToken);

    }
}