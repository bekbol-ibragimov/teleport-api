namespace Web.UseCases.Wrappers
{
    public class Error
    {
        public string Property { get; set; }

        /// <summary>
        /// Ниименование поля
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Сообщение об ошибке
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Является ли ошибка критичной
        /// </summary>
        public bool Critical { get; set; } = true;
    }
}