﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Utils.Enums;

namespace Utils.Pagination
{
    public static class QueryableSort
    {
        public static IQueryable<TEntity> OrderBy<TEntity, TSortField>(this IQueryable<TEntity> source, BaseSortFilter<TSortField> dto)
        where TSortField: struct, Enum
        {
            if (dto.SortField == null) return source;
            
            var command = dto.SortDirection == SortDirection.Descending ? "OrderByDescending" : "OrderBy";
            var type = typeof(TEntity);
            var property = type.GetProperty(dto.SortField.ToString());
            if(property == null) throw ApiException.NotFound(nameof(dto.SortField));
            var parameter = Expression.Parameter(type, "p");
            var propertyAccess = Expression.MakeMemberAccess(parameter, property);
            var orderByExpression = Expression.Lambda(propertyAccess, parameter);
            var resultExpression = Expression.Call(typeof(Queryable), command, new Type[] {type, property.PropertyType},
                source.Expression, Expression.Quote(orderByExpression));
            return source.Provider.CreateQuery<TEntity>(resultExpression);
        }

        public static async Task<PaginationResponse<T>> ToPaginationListAsync<T, TSortField>(
            this IQueryable<T> q, PaginationRequest<TSortField> request)
        where TSortField: struct, Enum
        {
            var count = await q.CountAsync();
            var queryLimit = q
                .Skip((request.PageNumber - 1) * request.PageSize)
                .Take(request.PageSize);
            var items = await queryLimit.ToListAsync();
            var result = new PaginationResponse<T>()
            {
                Total = count, 
                Items = items,
                PageNumber = request.PageNumber,
                PageSize = request.PageSize,
                SortField = request.SortField,
                SortDirection = request.SortDirection
            };
            return result;
        }
    }
}