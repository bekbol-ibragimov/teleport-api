﻿using System.Threading.Tasks;
using AutoMapper;
using GeoCoordinatePortable;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Web.Controllers.Common;
using WebApp.Interfaces.Services;


namespace Web.Controllers
{
    [AllowAnonymous]
    public class TestController: BaseController
    {
        private readonly IMapper _mapper;


        public TestController(IMapper mapper)
        {
            _mapper = mapper;
        }
        

        [HttpGet("[action]")]
        public double GeaCordinate()
        {
            var coord1 = new GeoCoordinate(53.477724, -2.232121);
            var coord2 = new GeoCoordinate(53.478121, -2.231105);

            var dist = coord1.GetDistanceTo(coord2);
            return dist;
        }
    }
}