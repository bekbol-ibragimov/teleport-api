﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ApplicationService.Interfaces.Auth;
using DataAccess.Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Utils;
using Web.UseCases.Auth.Account.Dto;

namespace Web.UseCases.Auth.Account.Commands
{
    public class AccountLoginSignCommand : IRequest<AuthDto>
    {
        public LoginSignDto Dto { get; set; }
        public string  Ip { get; set; }
        
        public class Handler : AccountBaseHandler<AccountLoginSignCommand, AuthDto>
        {
            private readonly IDbContext _dbContext;
            private readonly ISignFileControlService _signFileControl;
            private readonly ISettingHashingService _settingHashing;

            public Handler(IDbContext dbContext, IConfiguration config, 
                ISignFileControlService signFileControl, ISettingHashingService settingHashing) 
                : base(config, dbContext)
            {
                _dbContext = dbContext;
                _signFileControl = signFileControl;
                _settingHashing = settingHashing;
            }

            public override async Task<AuthDto> Handle(AccountLoginSignCommand request, CancellationToken cancellationToken)
            {
                var verifySignResult = await _signFileControl.ValidateSign(request.Dto.SignedString);
            
                if (verifySignResult.Status != 0 || verifySignResult.Result?.Signers.FirstOrDefault()?.Cert == null || verifySignResult.Result?.Signers.FirstOrDefault()?.Cert.Valid == false )
                {
                    throw ApiException.BadRequest(_signFileControl.GetSingErrorMessage(verifySignResult.Status));
                }
                var extract = await _signFileControl.ExtractRaw(request.Dto.SignedString);
                var decrypt = _settingHashing.Decrypto(extract.Data);
            
                var user =  await _dbContext.AuthUsers
                    .AsNoTracking()
                    .FirstOrDefaultAsync(u => u.Iin == verifySignResult.Result.Signers.FirstOrDefault().Cert.Subject.Iin, cancellationToken: cancellationToken);
            
                if (user == null || decrypt != request.Ip)
                {
                    throw ApiException.NotFound(nameof(user));
                }
            
                return await GenerateJwToken(user);
            }
        
 
        }
    }
}