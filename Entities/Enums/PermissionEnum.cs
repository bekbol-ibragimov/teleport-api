﻿﻿namespace Entities.Enums
{
    public enum PermissionEnum
    {
        /// <summary>
        /// Суперадмин
        /// </summary>
        Sa,
        /// <summary>
        /// Админ
        /// </summary>
        Admin,
        /// <summary>
        /// Пользователь
        /// </summary>
        User
    }
}
