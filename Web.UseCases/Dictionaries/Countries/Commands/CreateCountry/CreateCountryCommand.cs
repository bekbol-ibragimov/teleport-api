﻿using System;
using MediatR;
using Web.UseCases.Dictionaries.Countries.Dto;

namespace Web.UseCases.Dictionaries.Countries.Commands.CreateCountry
{
    public class CreateCountryCommand : IRequest<Guid>
    {
        public CreateCountryDto Dto { get; set; }
    }
}