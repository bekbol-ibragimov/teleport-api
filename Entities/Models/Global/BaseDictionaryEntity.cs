using System.ComponentModel.DataAnnotations;
using Entities.Attribute;

namespace Entities.Models.Global
{
    public class BaseDictionaryEntity : BaseEntity
    {
        [Required]
        [UniqueKey(groupId: "1", order: 0)]
        public string Code { get; set; }

        [Required]
        public string NameRu { get; set; }
        public string NameKz { get; set; }
        public string NameEn { get; set; }
    }
}