﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Web.Controllers.Common;
using Web.UseCases.Airports.Dto;
using Web.UseCases.Airports.Queries;
using Web.UseCases.Files.FileTemps.Queries;

namespace Web.Controllers.Files
{
    [AllowAnonymous]
    public class AirportController: BaseController
    {
        private readonly ISender _sender;
        public AirportController(ISender sender)
        {
            _sender = sender;
        }
        
        [HttpGet("[action]")]
        public async Task<DistanceOdto> GetDistance([Required] [RegularExpression("^[A-Z]{3}$")] string airport1, [Required] [RegularExpression("^[A-Z]{3}$")]  string airport2)
        {
            var result = await _sender.Send(new AirportGetDistance{Airport1 = airport1, Airport2 = airport2});
            return result;
        }


    }
}