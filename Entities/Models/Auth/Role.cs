﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Entities.Models.Global;

namespace Entities.Models.Auth
{
    public class Role: BaseEntity
    {
        public Role()
        {
            UserRoles = new HashSet<UserRole>();
            RolePermissions = new HashSet<RolePermission>();
        }

        [Required]
        public string NameRu { get; set; }
        public string NameKz { get; set; }
        public string NameEn { get; set; }
        
        
        //--------------------------------------------------------------------------------------------------------------
        public IEnumerable<UserRole> UserRoles { get; set; }
        public IEnumerable<RolePermission> RolePermissions { get; set; }
    }
}