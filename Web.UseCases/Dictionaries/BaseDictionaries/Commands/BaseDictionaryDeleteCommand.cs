﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using DataAccess.Interfaces;
using Entities.Models.Global;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Utils;
using Web.UseCases.Base;
using Web.UseCases.Dictionaries.BaseDictionaries.Dto;

namespace Web.UseCases.Dictionaries.BaseDictionaries.Commands
{
    public abstract class BaseDictionaryDeleteCommand : IRequest<Unit>
    {
        public Guid Id { get; set; }
        
        
        public class Handler<TRequest, TEntity> : BaseDictionaryHandler<TRequest, Unit, TEntity>
            where TRequest: BaseDictionaryDeleteCommand
            where TEntity : BaseDictionaryEntity
        {
            private readonly IMapper _mapper;
            private readonly IDbContext _dbContext;
            private readonly IQueryable<TEntity> _query;

            protected Handler(IMapper mapper, IDbContext dbContext): base(mapper, dbContext)
            {
                _mapper = mapper;
                _dbContext = dbContext;
                _query = Repo.Where(x => x.IsDeleted == false);
            }

            public override async Task<Unit> Handle(TRequest request, CancellationToken cancellationToken)
            {
                var entity = await _query.FirstOrDefaultAsync(x => x.Id == request.Id, cancellationToken);
                if(entity == null)
                    throw ApiException.NotFound();
                entity.IsDeleted = true;
                await _dbContext.SaveChangesAsync(cancellationToken);
                return Unit.Value;
            }
        }
    }
    
}