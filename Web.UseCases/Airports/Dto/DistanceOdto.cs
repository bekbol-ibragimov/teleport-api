﻿namespace Web.UseCases.Airports.Dto
{
    public class DistanceOdto
    {
        public double Meter { get; set; }
        public double Mile { get; set; }
    }
}