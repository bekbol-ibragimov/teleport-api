﻿using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Web.UseCases.Auth.Account.Commands;
using Web.UseCases.Auth.Account.Dto;

namespace WebApi.Swagger
{
    [ApiController]
    [Route("api/[controller]")]
    public class SwaggerController : ControllerBase
    {
        private readonly ISender _sender;
    
        public SwaggerController(ISender sender)
        {
            _sender = sender;
        }
    
        [HttpPost("Token")]
        [ApiExplorerSettings(IgnoreApi = true)]
        public async Task<IActionResult> Token([FromForm] LoginDto dto)
        {
            var result = await _sender.Send(new AccountLoginCommand {Dto = dto});//_authLogic.Authenticate(dto);
    
            if (string.IsNullOrEmpty(result.Token))
                return Unauthorized();
    
            return Ok(new
            {
                access_token = result.Token,
                refresh_token = result.Token
            });
        }
    }
}