﻿using Utils.Pagination;

namespace Web.UseCases.Auth.User.Dto
{
    public class UserFilterDto: PaginationRequest<UserOrderBy>
    {
        public string LastName { get; set; }
    }

    public enum UserOrderBy
    {
        LastName,
        CreatedAt,
        UpdatedAt
    }
}