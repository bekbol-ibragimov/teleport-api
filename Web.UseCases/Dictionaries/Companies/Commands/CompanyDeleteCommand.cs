﻿using AutoMapper;
using DataAccess.Interfaces;
using Entities.Models.Dictionaries;
using Web.UseCases.Dictionaries.BaseDictionaries.Commands;

namespace Web.UseCases.Dictionaries.Companies.Commands
{
    public class CompanyDeleteCommand: BaseDictionaryDeleteCommand
    {
        public class Handler : Handler<CompanyDeleteCommand, Company>
        {
            public Handler(IMapper mapper, IDbContext dbContext): base(mapper, dbContext)
            {
                
            }
        }
    }
}