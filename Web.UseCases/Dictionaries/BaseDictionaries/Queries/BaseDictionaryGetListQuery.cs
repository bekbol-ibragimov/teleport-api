﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using DataAccess.Interfaces;
using Entities.Models.Global;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Utils.Pagination;
using Web.UseCases.Base;
using Web.UseCases.Dictionaries.BaseDictionaries.Dto;

namespace Web.UseCases.Dictionaries.BaseDictionaries.Queries
{
    public class BaseDictionaryGetListQuery<TOdto1> : IRequest<IEnumerable<TOdto1>>
        where TOdto1 : BaseDictionaryOdto
    {
        public BaseDictionaryFilter Filter { get; set; }
        
        public class Handler<TRequest, TEntity, TOdto> : BaseDictionaryHandler<TRequest, IEnumerable<TOdto>, TEntity>
            where TRequest: BaseDictionaryGetListQuery<TOdto>
            where TEntity : BaseDictionaryEntity
            where TOdto : BaseDictionaryOdto
        {
            private readonly IMapper _mapper;
            private readonly IQueryable<TEntity> _query;

            public Handler(IMapper mapper, IDbContext dbContext): base(mapper, dbContext)
            {
                _mapper = mapper;
                _query = Repo.Where(x => x.IsDeleted == false);
            }

            public override async Task<IEnumerable<TOdto>> Handle(TRequest request, CancellationToken cancellationToken)
            {
                var result = await _query
                    .Where( x => string.IsNullOrEmpty(request.Filter.SearchString) || 
                                 x.Code.ToLower().Contains(request.Filter.SearchString.ToLower()) ||
                                 x.NameRu.ToLower().Contains(request.Filter.SearchString.ToLower()) || 
                                 x.NameKz.ToLower().Contains(request.Filter.SearchString.ToLower()) || 
                                 x.NameEn.ToLower().Contains(request.Filter.SearchString.ToLower()))
                    .AsNoTracking()
                    .OrderBy(request.Filter)
                    .ProjectTo<TOdto>(_mapper.ConfigurationProvider)
                    .ToListAsync(cancellationToken);
                return result;
            }
        }
    }
    

    
    
}