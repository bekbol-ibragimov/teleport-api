﻿using AutoMapper;
using DataAccess.Interfaces;
using Entities.Models.Dictionaries;
using Web.UseCases.Dictionaries.BaseDictionaries.Queries;

namespace Web.UseCases.Dictionaries.Companies.Queries
{
    public class CompanyCheckExistenceCodeQuery: BaseDictionaryCheckExistenceCodeQuery
    {
        public class Handler : Handler<CompanyCheckExistenceCodeQuery, Company>
        {
            public Handler(IMapper mapper, IDbContext dbContext): base(mapper, dbContext)
            {
                
            }
        }
    }
    

}