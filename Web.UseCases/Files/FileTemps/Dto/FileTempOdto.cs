﻿﻿using System;

 namespace Web.UseCases.Files.FileTemps.Dto
{
    public class FileTempOdto
    {
        public Guid Id { get; set; }
        public string Path { get; set; }
    }
}