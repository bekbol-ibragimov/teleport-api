﻿using AutoMapper;
using Entities.Models.Auth;
using Entities.Models.Dictionaries;
using Web.UseCases.Auth.User.Dto;
using Web.UseCases.Dictionaries.Companies.Dto;
using Web.UseCases.Dictionaries.Localizations.Dto;

namespace Web.UseCases
{
    public class WebUseCasesMapperProfile : Profile
    {
        public WebUseCasesMapperProfile()
        {
            #region Dictionary
      
            CreateMap<Company, CompanyOdto>();
            CreateMap<CompanyIdto, Company>().ReverseMap();
            
            CreateMap<Localization, LocalizationOdto>();
            CreateMap<LocalizationIdto, Localization>();
            
            #endregion

            UserMapped();
            
        }


        private void UserMapped()
        {
            CreateMap< UserCreateDto, User>();
            CreateMap< UserUpdateDto, User>();
            CreateMap< User, UserDto>();
        }
    }
}