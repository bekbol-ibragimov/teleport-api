﻿namespace Web.UseCases.Airports.Dto
{
    public class AirportDto
    {
        public string Country { get; set; }
        public string CityIata { get; set; }
        public string Iata { get; set; }
        public string City { get; set; }
        public string TimezoneRegionName { get; set; }
        public string CountryIata { get; set; }
        
        
        public string Name { get; set; }
        public LocationDto Location { get; set; }
        public string Type { get; set; }
        public int Hubs { get; set; }
    }
}