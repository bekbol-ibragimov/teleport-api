﻿
using System;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using DataAccess.Interfaces;
using Entities.Models.Files;
using MediatR;
using Microsoft.AspNetCore.Hosting;
using Utils;
using Web.UseCases.Base;
using Web.UseCases.Files.FileTemps.Command;
using Web.UseCases.Files.FileTemps.Dto;
using WebApp.Interfaces.Services;

namespace Web.UseCases.Files.FileTemps.Queries
{
    public class FileTempGetByIdQuery : IRequest<FileOdto>
    {
        public Guid Id { get; set; }

        public class Handler : BaseHandler<FileTempGetByIdQuery, FileOdto>
        {
            private readonly string _rootPath;

            public Handler(IMapper mapper, IDbContext dbContext, IHttpContextService httpContextService, IWebHostEnvironment environment) 
                : base(mapper, dbContext, httpContextService)
            {
                _rootPath = environment.WebRootPath;
            }

            public override async Task<FileOdto> Handle(FileTempGetByIdQuery request, CancellationToken cancellationToken)
            {
                var fileTemp = _dbContext.FileTemps.FirstOrDefault(x => x.Id == request.Id);
                if (fileTemp == null)
                    throw ApiException.NotFound(nameof(FileTemp));
                var result = new FileOdto
                {
                    File = await File.ReadAllBytesAsync(_rootPath + fileTemp.FullPath, cancellationToken),
                    Name = fileTemp.OriginalFileName
                };
                return result;
            }
            

        }
    }
}