using System.ComponentModel.DataAnnotations;

namespace Web.UseCases.Auth.Account.Dto
{
    public class LoginSignDto
    {
        [Required(ErrorMessage = "Подписанная строка не передана")]
        public string SignedString { get; set; }
    }
}