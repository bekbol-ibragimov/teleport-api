﻿using System.Threading.Tasks;

namespace WebApp.Interfaces.Services
{
    public interface ITeleportClient
    {
        Task<TResponse> SendAsync<TResponse>(string airport) where TResponse: class;
    }
}