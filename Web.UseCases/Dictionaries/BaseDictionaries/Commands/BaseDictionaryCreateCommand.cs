﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using DataAccess.Interfaces;
using Entities.Models.Global;
using MediatR;
using Utils;
using Web.UseCases.Base;
using Web.UseCases.Dictionaries.BaseDictionaries.Dto;

namespace Web.UseCases.Dictionaries.BaseDictionaries.Commands
{
    public abstract class BaseDictionaryCreateCommand<TIdto1, TOdto1> : IRequest<TOdto1>
        where TIdto1: BaseDictionaryIdto
        where TOdto1: BaseDictionaryOdto
    {
        public TIdto1 Idto { get; set; }
        
        public class Handler<TRequest, TEntity, TIdto, TOdto> : BaseDictionaryHandler<TRequest, TOdto, TEntity>
            where TRequest: BaseDictionaryCreateCommand<TIdto, TOdto>
            where TEntity : BaseDictionaryEntity 
            where TIdto : BaseDictionaryIdto
            where TOdto : BaseDictionaryOdto
        {
            private readonly IMapper _mapper;
            private readonly IDbContext _dbContext;
            private readonly IQueryable<TEntity> _query;

            protected Handler(IMapper mapper, IDbContext dbContext): base(mapper, dbContext)
            {
                _mapper = mapper;
                _dbContext = dbContext;
                _query = Repo.Where(x => x.IsDeleted == false);
            }

            public override async Task<TOdto> Handle(TRequest request, CancellationToken cancellationToken)
            {
                if (_query.Any(x => x.Code == request.Idto.Code)) { 
                    throw ApiException.Conflict(nameof(request.Idto.Code));
                }
                TEntity entity = _mapper.Map<TEntity>(request.Idto);
                Repo.Add(entity);
                await _dbContext.SaveChangesAsync(cancellationToken);
            
                return _mapper.Map<TOdto>(entity);;
            }
        }
    }
    
    
}