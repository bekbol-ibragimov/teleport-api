﻿using System.Threading;
using System.Threading.Tasks;
using Entities.Models;
using Entities.Models.Auth;
using Entities.Models.Dictionaries;
using Entities.Models.Files;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Interfaces
{
    public interface IDbContext
    {
        Task<int> SaveChangesAsync(CancellationToken token = default);

        #region Auth
        DbSet<User> AuthUsers { get; set; }
        DbSet<UserRole> AuthUserRoles { get; set; }
        DbSet<UserPermission> AuthUserPermissions { get; set; }
        DbSet<Role> AuthRoles { get; set; }
        DbSet<RolePermission> AuthRolePermissions { get; set; }
        DbSet<Permission> AuthPermissions { get; set; }
        

        #endregion Auth

        #region Dictionary

        DbSet<Company> DicCompanies { get; set; }
        DbSet<Country> DicCountries { get; set; }
        DbSet<Localization> DicLocalizations { get; set; }
        #endregion
        

        #region Files
        DbSet<FileTemp> FileTemps { get; set; }
        #endregion Files
        
        DbSet<TestEntity> TestEntities { get; set; }
        DbSet<NFox> NFoxes { get; set; }
        DbSet<Limit> Limits { get; set; }

        DbSet<TEntity> Set<TEntity>() where TEntity : class;
    }
}