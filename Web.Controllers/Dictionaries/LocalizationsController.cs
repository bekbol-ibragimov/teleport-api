﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Web.UseCases.Dictionaries.Localizations.Commands;
using Web.UseCases.Dictionaries.Localizations.Dto;
using Web.UseCases.Dictionaries.Localizations.Queries;
using Web.UseCases.Wrappers;
using WebApp.Interfaces.Enums;

namespace Web.Controllers.Dictionaries
{
    public class LocalizationsController: BaseDictionaryController<LocalizationIdto, LocalizationOdto,
    LocalizationGetListQuery, LocalizationGetByIdQuery, LocalizationCheckExistenceCodeQuery,
    LocalizationCreateCommand, LocalizationUpdateCommand, LocalizationDeleteCommand>
    {
        private readonly ISender _sender;
        public LocalizationsController(ISender sender): base(sender)
        {
            _sender = sender;
        }
        
        [AllowAnonymous]
        [HttpGet("GetTranslations/{lang}")]
        public async Task<ApiResponse<Dictionary<string, string>>> GetTranslations (Lang lang)
        {
            var result = await _sender.Send(new LocalizationGetTranslationsCommand {Lang = lang});
            return ResponseOk(result);

        }
        
    }
}