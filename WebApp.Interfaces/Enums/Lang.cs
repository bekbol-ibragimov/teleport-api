﻿namespace WebApp.Interfaces.Enums
{
    public enum Lang
    {
        Ru = 1,
        Kz = 2,
        En = 3,
        Code = 4,
    }
}