﻿using AutoMapper;
using DataAccess.Interfaces;
using Entities.Models.Dictionaries;
using Web.UseCases.Dictionaries.BaseDictionaries.Queries;
using Web.UseCases.Dictionaries.Localizations.Dto;

namespace Web.UseCases.Dictionaries.Localizations.Queries
{
    public class LocalizationGetListQuery: BaseDictionaryGetListQuery<LocalizationOdto>
    {
        public class Handler : Handler<LocalizationGetListQuery, Localization, LocalizationOdto>
        {
            public Handler(IMapper mapper, IDbContext dbContext): base(mapper, dbContext)
            {
                
            }
        }
    }
}