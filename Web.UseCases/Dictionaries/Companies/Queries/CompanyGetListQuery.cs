﻿using AutoMapper;
using DataAccess.Interfaces;
using Entities.Models.Dictionaries;
using Web.UseCases.Dictionaries.BaseDictionaries.Queries;
using Web.UseCases.Dictionaries.Companies.Dto;

namespace Web.UseCases.Dictionaries.Companies.Queries
{
    public class CompanyGetListQuery: BaseDictionaryGetListQuery<CompanyOdto>
    {
        public class Handler : Handler<CompanyGetListQuery, Company, CompanyOdto>
        {
            public Handler(IMapper mapper, IDbContext dbContext): base(mapper, dbContext)
            {
                
            }
        }
    }
}