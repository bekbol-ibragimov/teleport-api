﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using DataAccess.Interfaces;
using Entities.Models.Global;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Web.UseCases.Base;
using Web.UseCases.Dictionaries.BaseDictionaries.Dto;

namespace Web.UseCases.Dictionaries.BaseDictionaries.Queries
{
    public abstract class BaseDictionaryCheckExistenceCodeQuery: IRequest<bool>
    {
        public string Code { get; set; }
        
        public class Handler<TRequest, TEntity> : BaseDictionaryHandler<TRequest, bool, TEntity>
            where TRequest: BaseDictionaryCheckExistenceCodeQuery
            where TEntity : BaseDictionaryEntity
        {
            private readonly IQueryable<TEntity> _query;

            public Handler(IMapper mapper, IDbContext dbContext): base(mapper, dbContext)
            {
                _query = Repo.Where(x => x.IsDeleted == false);
            }
        
            public override async Task<bool> Handle(TRequest request, CancellationToken cancellationToken)
            {
                return await _query.AnyAsync(x => x.Code == request.Code, cancellationToken: cancellationToken);
            }
        }
    }
}