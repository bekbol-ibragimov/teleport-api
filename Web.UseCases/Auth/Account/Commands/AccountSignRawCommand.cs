﻿using System.Threading;
using System.Threading.Tasks;
using ApplicationService.Interfaces.Auth;
using MediatR;

namespace Web.UseCases.Auth.Account.Commands
{
    public class AccountSignRawCommand : IRequest<string>
    {
        public string Ip { get; set; }
        
        public class Handler : IRequestHandler<AccountSignRawCommand, string>
        {
            private readonly ISettingHashingService _settingHashing;

            public Handler(ISettingHashingService settingHashing)
            {
                _settingHashing = settingHashing;
            }

            public async Task<string> Handle(AccountSignRawCommand request, CancellationToken cancellationToken)
            {
                var res =  _settingHashing.Crypto(request.Ip);
                return await Task.Run(()=> res, cancellationToken);
            }

        }
    }
}