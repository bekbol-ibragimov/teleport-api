﻿namespace Web.UseCases.Files.FileTemps.Dto
{
    public class FileOdto
    {
        public byte[] File { get; set; }
        
        public string Name { get; set; }
    }
}