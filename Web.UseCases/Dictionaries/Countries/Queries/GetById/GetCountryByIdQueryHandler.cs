﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using DataAccess.Interfaces;
using Entities.Models.Dictionaries;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Web.UseCases.Dictionaries.Countries.Dto;

namespace Web.UseCases.Dictionaries.Countries.Queries.GetById
{
    public class GetCountryByIdQueryHandler : IRequestHandler<GetCountryByIdQuery, CountryDto>
    {
        private readonly IMapper _mapper;
        private readonly IDbContext _dbContext;

        public GetCountryByIdQueryHandler(IMapper mapper, IDbContext dbContext)
        {
            _mapper = mapper;
            _dbContext = dbContext;
        }

        public async Task<CountryDto> Handle(GetCountryByIdQuery request, CancellationToken cancellationToken)
        {
            var country = await _dbContext.DicCountries
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.IsDeleted == false && x.Id == request.Id, cancellationToken);
            if (country == null) throw new EntityNotFoundException();
            return _mapper.Map<CountryDto>(country);
        }
    }
}