﻿namespace Web.UseCases.Airports.Dto
{
    public class LocationDto
    {
        public double Lon { get; set; }
        public double Lat { get; set; }
    }
}