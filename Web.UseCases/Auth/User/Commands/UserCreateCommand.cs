﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using DataAccess.Interfaces;
using MediatR;
using Web.UseCases.Auth.User.Dto;
using Web.UseCases.Base;
using WebApp.Interfaces.Services;
using Entities.Models.Auth;
using Utils;

namespace Web.UseCases.Auth.User.Commands
{
    public class UserCreateCommand: IRequest<Guid>
    {
        public UserCreateDto Dto { get; set; }
        
        public class Handler: BaseHandler<UserCreateCommand, Guid>
        {
            public Handler(IMapper mapper, IDbContext dbContext, IHttpContextService httpContextService) : base(mapper, dbContext, httpContextService)
            {
            }

            public override async Task<Guid> Handle(UserCreateCommand request, CancellationToken cancellationToken)
            {
                var user = _mapper.Map<Entities.Models.Auth.User>(request.Dto);
                user.PasswordHash = HashUtils.DoHash(request.Dto.Password);
                await _dbContext.AuthUsers.AddAsync(user, cancellationToken);
                await _dbContext.SaveChangesAsync(cancellationToken);
                return user.Id;
            }
        }
    }
}