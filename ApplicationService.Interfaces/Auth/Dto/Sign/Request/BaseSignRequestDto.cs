using Newtonsoft.Json;

namespace ApplicationService.Interfaces.Auth.Dto.Sign.Request
{
    public class BaseSignRequestDto
    {
        [JsonProperty("version")] public string Version { get; set; }
        [JsonProperty("method")] public string Method { get; set; }
    }
}