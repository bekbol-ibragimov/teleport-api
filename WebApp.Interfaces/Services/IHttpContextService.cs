﻿using System;
using WebApp.Interfaces.Enums;

namespace WebApp.Interfaces.Services
{
    public interface IHttpContextService
    {
        Guid UserId { get; }
        Lang Lang { get; }
    }
}