﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entities.Models
{
    /// <summary>
    /// Ограничения на транспортные средства
    /// </summary>
    [Table("LIMITS")]
    public class Limit
    {
        [Column("ID")]
        public int Id { get; set; }
        /// <summary>
        /// Исходящий номер (при снятии)
        /// </summary>
        [Column("OUTERNUM")]
        public string OuterNum { get; set; }
        /// <summary>
        /// От какого числа (при снятии)
        /// </summary>
        [Column("OUTERDATE")]
        public DateTime OuterDate { get; set; }
        /// <summary>
        /// Орган, наложивший обременение
        /// </summary>
        [Column("ORGAN")]
        public string Organ { get; set; }
        /// <summary>
        /// Входящий номер
        /// </summary>
        [Column("INNERNUM")]
        public string InnerNum { get; set; }
        /// <summary>
        /// От какого числа
        /// </summary>
        [Column("INNERDATE")]
        public DateTime InnerDate { get; set; }
        /// <summary>
        /// Основание наложения обременения
        /// </summary>
        [Column("INBASE")]
        public string InBase { get; set; }
        /// <summary>
        /// Дата ввода
        /// </summary>
        [Column("INDATE")]
        public DateTime InDate { get; set; }
        /// <summary>
        /// Оператор ввода
        /// </summary>
        [Column("INOPERATOR")]
        public string InOperator { get; set; }
        /// <summary>
        /// Дата снятия
        /// </summary>
        [Column("SNDATE")]
        public DateTime SnDate { get; set; }
        /// <summary>
        /// Основание снятия
        /// </summary>
        [Column("SNBASE")]
        public string SnBase { get; set; }
        /// <summary>
        /// Оператор снятия
        /// </summary>
        [Column("SNOPERATOR")]
        public string SnOperator { get; set; }
        /// <summary>
        /// Состояние обременения (0-снято/1-Активно)
        /// </summary>
        [Column("FLAG")]
        public string Flag { get; set; }
        /// <summary>
        /// Тип обременения (от 1 до 5)
        /// </summary>
        [Column("TIP")]
        public string Tip { get; set; }
        /// <summary>
        /// MAC-адрес АРМ
        /// </summary>
        [Column("MAC")]
        public int Mac  { get; set; }
        /// <summary>
        /// IP-адрес АРМ
        /// </summary>
        [Column("IP")]
        public string Ip  { get; set; }
        /// <summary>
        /// Код региона. КАТО
        /// </summary>
        [Column("REGION_ID")]
        public string RegionId  { get; set; }
        /// <summary>
        /// Оператор корректировки
        /// </summary>
        [Column("EDOPERATOR")]
        public string EditOperator  { get; set; }
        /// <summary>
        /// Дата корректировки
        /// </summary>
        [Column("EDDATE")]
        public DateTime EditDate  { get; set; }
        /// <summary>
        /// Код оператора корректировки
        /// </summary>
        [Column("EDOPCODE")]
        public int EditOperatorCode  { get; set; }
        /// <summary>
        /// Код оператора ввода
        /// </summary>
        [Column("INOPCODE")]
        public int InOperatorCode  { get; set; }
        /// <summary>
        /// Код оператора снятия
        /// </summary>
        [Column("SNOPCODE")]
        public int SnOperatorCode  { get; set; }
        /// <summary>
        /// Идентификатор записи в локальной базе
        /// </summary>
        [Column("ID_AIPS")]
        public int IdAips  { get; set; }
        /// <summary>
        /// Дата ввода
        /// </summary>
        [Column("DVV")]
        public DateTime Dvv  { get; set; }
        /// <summary>
        /// Дата корректировки
        /// </summary>
        [Column("DKR")]
        public DateTime Dkr  { get; set; }	
    }
}