﻿using System;
using System.Linq;
using System.Security.Claims;
using Entities.Enums;
using Entities.Models.Auth;
using Microsoft.AspNetCore.Http;
using Web.UseCases.Enums;
using WebApp.Interfaces.Enums;
using WebApp.Interfaces.Services;

namespace WebApi.Services
{
    public class HttpContextService : IHttpContextService
    {
        public Guid UserId { get; }
        private Lang _lang = Lang.Ru;
        private PermissionEnum[] _permissions;
        private readonly IHttpContextAccessor _httpContextAccessor;
        
        public HttpContextService(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
            Guid.TryParse(httpContextAccessor.HttpContext?.User?.FindFirstValue(ClaimType.UserId), out var userId);
            UserId = userId;
            
        }

        public Lang Lang
        {
            get
            {
                var request = _httpContextAccessor.HttpContext.Request;
                if (request == null) return _lang;
                string lang = request.Headers["Accept-Language"];
                var langs = Enum.GetNames(typeof(Lang)).Select(x => x.ToLower());
                if (langs.Contains(lang))
                    _lang = Enum.Parse<Lang>(lang,true);
                return _lang;
            }
        }
        public PermissionEnum[] Permissions
        {
            get
            {
                if (_permissions != null) return _permissions;
                var permissions = Enum.GetNames(typeof(PermissionEnum));
                _permissions = _httpContextAccessor.HttpContext?.User.Claims
                    .Where(x => x.Type == ClaimType.Permission && permissions.Contains(x.Value))
                    .Select(x => Enum.Parse<PermissionEnum>(x.Value) )
                    .ToArray();
                return _permissions;
            }
        }
    }
}