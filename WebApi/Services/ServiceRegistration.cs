﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using WebApp.Interfaces.Services;

namespace WebApi.Services
{
    public static class ServiceRegistration
    {
        public static void RegistrationServices(this IServiceCollection services, IConfiguration config)
        {
            services.AddScoped<IHttpContextService, HttpContextService>();
            services.AddTransient<IDateTimeService, DateTimeService>();
            services.AddHttpClient<ITeleportClient, TeleportClient>();
        }
    }
}