﻿using AutoMapper;
using DataAccess.Interfaces;
using Entities.Models.Dictionaries;
using Web.UseCases.Dictionaries.BaseDictionaries.Commands;

namespace Web.UseCases.Dictionaries.Localizations.Commands
{
    public class LocalizationDeleteCommand: BaseDictionaryDeleteCommand
    {
        public class Handler : Handler<LocalizationDeleteCommand, Localization>
        {
            public Handler(IMapper mapper, IDbContext dbContext): base(mapper, dbContext)
            {
                
            }
        }
    }
}