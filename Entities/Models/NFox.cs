﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entities.Models
{
    /// <summary>
    /// Транспортные средства
    /// </summary>
    [Table("NFOX")]
    public class NFox
    {
        /// <summary>
        /// порядковый номер записи в базе
        /// </summary>
        [Key]
        [Column("NN")]
        public long NN { get; set; }
        /// <summary>
        /// дата операции (постановки на учет/снятия с учета)
        /// </summary>
        [Column("F1")]
        public DateTime F1 { get; set; }
        /// <summary>
        /// гос. номерной знак
        /// </summary>
        [Column("F2")]
        public string F2 { get; set; }
        /// <summary>
        /// прежний гос. номерной знак
        /// </summary>
        [Column("F3")]
        public string F3 { get; set; }
        /// <summary>
        /// марка, модель, модификация ТС
        /// </summary>
        [Column("F4")]
        public string F4 { get; set; }
        /// <summary>
        /// год выпуска ТС
        /// </summary>
        [Column("F5")]
        public string F5 { get; set; }
        /// <summary>
        /// номер двигателя ТС
        /// </summary>
        [Column("F6")]
        public string F6 { get; set; }
        /// <summary>
        /// номер шасси ТС
        /// </summary>
        [Column("F7")]
        public string F7 { get; set; }
        /// <summary>
        /// номер кузова ТС (Vin-код)
        /// </summary>
        [Column("F8")]
        public string F8 { get; set; }
        /// <summary>
        /// цвет (код из справочника)
        /// </summary>
        [Column("F9")]
        public string F9 { get; set; }
        /// <summary>
        /// серия и номер СРТС
        /// </summary>
        [Column("F10")]
        public string F10 { get; set; }
        /// <summary>
        /// категория ТС («A», «A1», «B», «B1», «BE», «C», «C1», «C1E», «CE», «D», «D1», «D1E», «DE»)
        /// </summary>
        [Column("F11")]
        public string F11 { get; set; }
        /// <summary>
        /// мощность двигателя (кВт)
        /// </summary>
        [Column("F12")]
        public string F12 { get; set; }
        /// <summary>
        /// мощность двигателя (л.с.)
        /// </summary>
        [Column("F13")]
        public string F13 { get; set; }
        /// <summary>
        /// объем двигателя (куб. см)
        /// </summary>
        [Column("F14")]
        public string F14 { get; set; }
        /// <summary>
        /// разрешенная максимальная масса
        /// </summary>
        [Column("F15")]
        public string F15 { get; set; }
        /// <summary>
        /// масса без нагрузки
        /// </summary>
        [Column("F16")]
        public string F16 { get; set; }
        /// <summary>
        /// код документа, подтверждающего законность приобретения ТС («A» – «M»)
        /// </summary>
        [Column("F17")]
        public string F17 { get; set; }
        /// <summary>
        /// номер документа, подтверждающего законность приобретения ТС, дата его выдачи
        /// </summary>
        [Column("F18")]
        public string F18 { get; set; }
        /// <summary>
        /// фамилия / наименование юридического лица
        /// </summary>
        [Column("F19")]
        public string F19 { get; set; }
        /// <summary>
        /// имя / код юридического лица
        /// </summary>
        [Column("F20")]
        public string F20 { get; set; }
        /// <summary>
        /// отчество / ведомство юридического лица
        /// </summary>
        [Column("F21")]
        public string F21 { get; set; }
        /// <summary>
        /// дата рождения (только для физического лица)
        /// </summary>
        [Column("F22")]
        public string F22 { get; set; }
        /// <summary>
        /// серия документа, подтверждающего личность владельца ТС (только для физического лица)
        /// </summary>
        [Column("F23")]
        public string F23 { get; set; }
        /// <summary>
        /// номер документа, подтверждающего личность владельца ТС (только для физического лица)
        /// </summary>
        [Column("F24")]
        public string F24 { get; set; }
        /// <summary>
        /// серия водительского удостоверения владельца СРТС / водителя
        /// </summary>
        [Column("F25")]
        public string F25 { get; set; }
        /// <summary>
        /// номер водительского удостоверения владельца СРТС / водителя
        /// </summary>
        [Column("F26")]
        public string F26 { get; set; }
        /// <summary>
        /// область местожительства / местонахождения владельца СРТС (код из справочника)
        /// </summary>
        [Column("F27")]
        public string F27 { get; set; }
        /// <summary>
        /// район местожительства / местонахождения владельца СРТС (код из справочника)
        /// </summary>
        [Column("F28")]
        public string F28 { get; set; }
        /// <summary>
        /// населенный пункт местожительства / местонахождения владельца СРТС
        /// </summary>
        [Column("F29")]
        public string F29 { get; set; }
        /// <summary>
        /// улица местожительства / местонахождения владельца СРТС
        /// </summary>
        [Column("F30")]
        public string F30 { get; set; }
        /// <summary>
        /// номер дома, корпус местожительства / местонахождения владельца СРТС
        /// </summary>
        [Column("F31")]
        public string F31 { get; set; }
        /// <summary>
        /// номер квартиры, комнаты местожительства/ местонахождения владельца СРТС
        /// </summary>
        [Column("F32")]
        public string F32 { get; set; }
        /// <summary>
        /// телефонный номер владельца СРТС
        /// </summary>
        [Column("F33")]
        public string F33 { get; set; }
        /// <summary>
        /// населенный пункт местонахождения гаража, стоянки (только для юридических лиц)
        /// </summary>
        [Column("F34")]
        public string F34 { get; set; }
        /// <summary>
        /// район местонахождения гаража, стоянки (код из справочника) (только для юридических лиц)
        /// </summary>
        [Column("F35")]
        public string F35 { get; set; }
        /// <summary>
        /// улица местонахождения гаража, стоянки (только для юридических лиц)
        /// </summary>
        [Column("F36")]
        public string F36 { get; set; }
        /// <summary>
        /// номер дома, корпуса местонахождения гаража, стоянки (только для юридических лиц)
        /// </summary>
        [Column("F37")]
        public string F37 { get; set; }
        /// <summary>
        /// номер бокса местонахождения гаража, стоянки (только для юридических лиц)
        /// </summary>
        [Column("F38")]
        public string F38 { get; set; }
        /// <summary>
        /// место работы владельца СРТС (только для физических лиц)
        /// </summary>
        [Column("F39")]
        public string F39 { get; set; }
        /// <summary>
        /// статус карточки ( «P»-на учете, «S»-снятая с учета)
        /// </summary>
        [Column("F40")]
        public string F40 { get; set; }
        /// <summary>
        /// код гос. номерного знака (3 цифры)
        /// </summary>
        [Column("F41")]
        public string F41 { get; set; }
        /// <summary>
        /// код прежнего гос. номерного знака (3 цифры)
        /// </summary>
        [Column("F42")]
        public string F42 { get; set; }
        /// <summary>
        /// юридическое («1») / физическое лицо («2»)
        /// </summary>
        [Column("F43")]
        public string F43 { get; set; }
        /// <summary>
        /// код типа ТС (6 цифр)
        /// </summary>
        [Column("F44")]
        public string F44 { get; set; }
        /// <summary>
        /// VIN код ТС 
        /// </summary>
        [Column("F45")]
        public string F45 { get; set; }
        /// <summary>
        /// код документа, подтверждающего личность владельца ТС («1»-паспорт, «2»-удостоверение) (только для физического лица)
        /// </summary>
        [Column("F46")]
        public string F46 { get; set; }
        /// <summary>
        /// дата выдачи документа, подтверждающего личность владельца ТС (только для физического лица)
        /// </summary>
        [Column("F47")]
        public DateTime F47 { get; set; }
        /// <summary>
        /// серия и номер прежнего СРТС
        /// </summary>
        [Column("F48")]
        public string F48 { get; set; }
        /// <summary>
        /// особые отметки
        /// </summary>
        [Column("F49")]
        public string F49 { get; set; }
        /// <summary>
        /// причина снятия с учета ТС («A»– «Z») только для снятых с учета ТС)
        /// </summary>
        [Column("F50")]
        public string F50 { get; set; }
        /// <summary>
        /// входящий номер (при снятии с учета) (только для снятых с учета ТС)
        /// </summary>
        [Column("F51")]
        public string F51 { get; set; }
        /// <summary>
        /// основание переоборудования
        /// </summary>
        [Column("F52")]
        public string F52 { get; set; }
        /// <summary>
        /// дата переоборудования
        /// </summary>
        [Column("F53")]
        public DateTime F53 { get; set; }
        /// <summary>
        /// дата распечатки СРТС
        /// </summary>
        [Column("F54")]
        public DateTime F54 { get; set; }
        /// <summary>
        /// фамилия оператора
        /// </summary>
        [Column("F55")]
        public string F55 { get; set; }
        /// <summary>
        /// фамилия инспектора
        /// </summary>
        [Column("F56")]
        public string F56 { get; set; }
        /// <summary>
        /// кол-во посадочных мест 
        /// </summary>
        [Column("F57")]
        public string F57 { get; set; }
        /// <summary>
        /// номер РЭО первичной постановки
        /// </summary>
        [Column("F58")]
        public string F58 { get; set; }
        /// <summary>
        /// количество распечаток
        /// </summary>
        [Column("F59")]
        public string F59 { get; set; }
        /// <summary>
        /// сведения о браке
        /// </summary>
        [Column("F60")]
        public string F60 { get; set; }
        /// <summary>
        /// код РЭО
        /// </summary>
        [Column("F61")]
        public string F61 { get; set; }
        /// <summary>
        /// фамилия доверенного лица
        /// </summary>
        [Column("F62")]
        public string F62 { get; set; }
        /// <summary>
        /// имя доверенного лица
        /// </summary>
        [Column("F63")]
        public string F63 { get; set; }
        /// <summary>
        /// отчество доверенного лица
        /// </summary>
        [Column("F64")]
        public string F64 { get; set; }
        /// <summary>
        /// дата рождения доверенного лица
        /// </summary>
        [Column("F65")]
        public DateTime F65 { get; set; }
        /// <summary>
        /// тип удостоверяющего личность документа доверенного лица
        /// </summary>
        [Column("F66")]
        public string F66 { get; set; }
        /// <summary>
        /// серия паспорта доверенного лица
        /// </summary>
        [Column("F67")]
        public string F67 { get; set; }
        /// <summary>
        /// номер паспорта доверенного лица
        /// </summary>
        [Column("F68")]
        public string F68 { get; set; }
        /// <summary>
        /// дата выдачи паспорта доверенного лица
        /// </summary>
        [Column("F69")]
        public string F69 { get; set; }
        /// <summary>
        /// код области проживания доверенного лица
        /// </summary>
        [Column("F70")]
        public string F70 { get; set; }
        /// <summary>
        /// код района проживания доверенного лица
        /// </summary>
        [Column("F71")]
        public string F71 { get; set; }
        /// <summary>
        /// населенный пункт проживания доверенного лица
        /// </summary>
        [Column("F72")]
        public string F72 { get; set; }
        /// <summary>
        /// улица проживания доверенного лица
        /// </summary>
        [Column("F73")]
        public string F73 { get; set; }
        /// <summary>
        /// номер дома проживания доверенного лица
        /// </summary>
        [Column("F74")]
        public string F74 { get; set; }
        /// <summary>
        /// номер квартиры проживания доверенного лица
        /// </summary>
        [Column("F75")]
        public string F75 { get; set; }
        /// <summary>
        /// телефонный номер доверенного лица
        /// </summary>
        [Column("F76")]
        public string F76 { get; set; }

        /// <summary>
        /// национальность доверенного лица (код)
        /// </summary>
        [Column("F81")]
        public string F81 { get; set; }
        /// <summary>
        /// пол доверенного лица («1»=м, «2»=ж)
        /// </summary>
        [Column("F82")]
        public string F82 { get; set; }        
        /// <summary>
        /// код документа, подтверждающего льготы
        /// </summary>
        [Column("F83")]
        public string F83 { get; set; }
        /// <summary>
        /// тип льготы («1» нет льгот, «2» скидка, «3» бесплатно)
        /// </summary>
        [Column("F84")]
        public string F84 { get; set; }
        /// <summary>
        /// серия, номер, дата выдачи документа, подтверждающего льготы
        /// </summary>
        [Column("F85")]
        public string F85 { get; set; }
        /// <summary>
        /// РНН
        /// </summary>
        [Column("F86")]
        public string F86 { get; set; }
        /// <summary>
        /// имя почтового файла (запросы на постановку)
        /// </summary>
        [Column("F87")]
        public string F87 { get; set; }
        /// <summary>
        /// наименование банка
        /// </summary>
        [Column("F88")]
        public string F88 { get; set; }
        /// <summary>
        /// наименование платёжного документа
        /// </summary>
        [Column("F89")]
        public string F89 { get; set; }
        /// <summary>
        /// номер документа, подтверждающего оплату за СРТС
        /// </summary>
        [Column("F90")]
        public string F90 { get; set; }
        /// <summary>
        /// дата оплаты
        /// </summary>
        [Column("F91")]
        public string F91 { get; set; }
        /// <summary>
        /// сумма оплаты
        /// </summary>
        [Column("F92")]
        public string F92 { get; set; }
        /// <summary>
        /// наименование банка
        /// </summary>
        [Column("F93")]
        public string F93 { get; set; }
        /// <summary>
        /// наименование платёжного документа
        /// </summary>
        [Column("F94")]
        public string F94 { get; set; }
        /// <summary>
        /// номер документа, подтверждающего оплату за ГРНЗ
        /// </summary>
        [Column("F95")]
        public string F95 { get; set; }
        /// <summary>
        /// дата оплаты
        /// </summary>
        [Column("F96")]
        public DateTime F96 { get; set; }
        /// <summary>
        /// сумма оплаты
        /// </summary>
        [Column("F97")]
        public string F97 { get; set; }
        /// <summary>
        /// флаг, определяющий новый или прежний ГРНЗ
        /// </summary>
        [Column("F98")]
        public string F98 { get; set; }
        /// <summary>
        /// тип ГРНЗ (1,2,1А,2А,…)
        /// </summary>
        [Column("F99")]
        public string F99 { get; set; }
        /// <summary>
        /// номер СРТС до отбраковки
        /// </summary>
        [Column("F100")]
        public string F100 { get; set; }
        /// <summary>
        /// ИИН (физ. Лицо) / БИН (юр. Лицо)
        /// </summary>
        [Column("F101")]
        public string F101 { get; set; }
        /// <summary>
        /// фамилия инспектора при снятии с учета
        /// </summary>
        [Column("F102")]
        public string F102 { get; set; }        
        /// <summary>
        /// фамилия поставившего на временный учет
        /// </summary>
        [Column("F103")]
        public string F103 { get; set; }
        /// <summary>
        /// серийный номер токена оператора при снятии
        /// </summary>
        [Column("F104")]
        public string F104 { get; set; }
        /// <summary>
        /// Категория VIP ГРНЗ (0, 1, 2, 3, 4, 5, 6, 7)
        /// </summary>
        [Column("F105")]
        public string F105 { get; set; }
        /// <summary>
        /// код нас. пункта КАТО владельца ТС
        /// </summary>
        [Column("F106")]
        public string F106 { get; set; }
        /// <summary>
        /// код нас. пункта КАТО доверенного лица
        /// </summary>
        [Column("F107")]
        public string F107 { get; set; }
        /// <summary>
        /// код нас. пункта КАТО адрес гаража
        /// </summary>
        [Column("F108")]
        public string F108 { get; set; }
        /// <summary>
        /// дата первичной регистрации ТС
        /// </summary>
        [Column("F109")]
        public DateTime F109 { get; set; }
        /// <summary>
        /// Техническая категория ТС
        /// </summary>
        [Column("F110")]
        public string F110 { get; set; }
        /// <summary>
        /// флаг показывающий, обязательна ли оплата для данного СРТС
        /// </summary>
        [Column("F111")]
        public string F111 { get; set; }
        /// <summary>
        /// имя почтового файла (не используется)
        /// </summary>
        [Column("F112")]
        public string F112 { get; set; }
        /// <summary>
        /// номер РЭО снятия с учета
        /// </summary>
        [Column("F113")]
        public string F113 { get; set; }
        /// <summary>
        /// идентификационный номер ТС (VIN) по базам КТК
        /// </summary>
        [Column("F114")]
        public string F114 { get; set; }
        /// <summary>
        /// номер кузова по базам КТК
        /// </summary>
        [Column("F115")]
        public string F115 { get; set; }
        /// <summary>
        /// марка, модель, модификация ТС по базам КТК
        /// </summary>
        [Column("F116")]
        public string F116 { get; set; }
        /// <summary>
        /// год выпуска ТС по базам КТК
        /// </summary>
        [Column("F117")]
        public string F117 { get; set; }
        /// <summary>
        /// ФИО получателя по базам КТК
        /// </summary>
        [Column("F118")]
        public string F118 { get; set; }
        /// <summary>
        /// номер шасси по базам КТК
        /// </summary>
        [Column("F119")]
        public string F119 { get; set; }
        /// <summary>
        /// регистрационный №ДТ по базам КТК
        /// </summary>
        [Column("F120")]
        public string F120 { get; set; }
        /// <summary>
        /// код области КАТО владельца ТС
        /// </summary>
        [Column("F121")]
        public string F121 { get; set; }
        /// <summary>
        /// код района КАТО владельца ТС
        /// </summary>
        [Column("F122")]
        public string F122 { get; set; }
        /// <summary>
        /// код населенного пункта КАТО владельца ТС
        /// </summary>
        [Column("F123")]
        public string F123 { get; set; }
        /// <summary>
        /// код области КАТО доверенного лица ТС
        /// </summary>
        [Column("F124")]
        public string F124 { get; set; }
        /// <summary>
        /// код района КАТО доверенного лица ТС
        /// </summary>
        [Column("F125")]
        public string F125 { get; set; }
        /// <summary>
        /// код населенного пункта КАТО доверенного лица ТС
        /// </summary>
        [Column("F126")]
        public string F126 { get; set; }
        /// <summary>
        /// код района КАТО 
        /// </summary>
        [Column("F127")]
        public string F127 { get; set; }
        /// <summary>
        /// код населенного пункта КАТО местонахождения гаража, стоянки (для юр. лиц)
        /// </summary>
        [Column("F128")]
        public string F128 { get; set; }
        /// <summary>
        /// код социального статуса (из справочника)
        /// </summary>
        [Column("F129")]
        public string F129 { get; set; }
        /// <summary>
        /// название, соответствующее коду социального статуса (рус)
        /// </summary>
        [Column("F130")]
        public string F130 { get; set; }
        /// <summary>
        /// название, соответствующее коду социального статуса (каз)
        /// </summary>
        [Column("F131")]
        public string F131 { get; set; }
        /// <summary>
        /// дата назначения социального статуса 
        /// </summary>
        [Column("F132")]
        public string F132 { get; set; }
        /// <summary>
        /// дата окончания социального статуса
        /// </summary>
        [Column("F133")]
        public string F133 { get; set; }
        /// <summary>
        /// особые отметки для печати на smartcard
        /// </summary>
        [Column("F134")]
        public string F134 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [Column("DVV")]
        public string DVV { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [Column("DKR")]
        public string DKR { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [Column("CLIENTCARD")]
        public string ClientCard { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [Column("SAMCARD")]
        public string SamCard { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [Column("GBD_DAT")]
        public string GBD_DAT { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [Column("APPVER")]
        public string APPVER { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [Column("AIPS_HEX")]
        public string AIPS_HEX { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [Column("B_RNION")]
        public string B_RNION { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [Column("F2F10")]
        public string F2F10 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [Column("F6_6")]
        public string F6_6 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [Column("F8_")]
        public string F8_ { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [Column("F8_6")]
        public string F8_6 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [Column("GUID_SC")]
        public string GUID_SC { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [Column("ID_SC")]
        public string ID_SC { get; set; }
        
        
        
        
        
    }
}