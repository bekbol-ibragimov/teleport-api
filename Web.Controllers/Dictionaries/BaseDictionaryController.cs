﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Entities.Enums;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Web.Controllers.Attributes;
using Web.Controllers.Common;
using Web.UseCases.Dictionaries.BaseDictionaries.Commands;
using Web.UseCases.Dictionaries.BaseDictionaries.Dto;
using Web.UseCases.Dictionaries.BaseDictionaries.Queries;
using Web.UseCases.Wrappers;

namespace Web.Controllers.Dictionaries
{
    [Route("api/Dictionaries/[controller]")]
    public abstract class BaseDictionaryController<TIdto, TOdto, TGetList, TGetById, TCheckExistenceCode, TCreate, TUpdate, TDelete> : BaseController
        where TIdto : BaseDictionaryIdto, new()
        where TOdto : BaseDictionaryOdto, new()
        where TGetList: BaseDictionaryGetListQuery<TOdto>, new()
        where TGetById: BaseDictionaryGetByIdQuery<TOdto>, new()
        where TCheckExistenceCode: BaseDictionaryCheckExistenceCodeQuery, new()
        where TCreate: BaseDictionaryCreateCommand<TIdto, TOdto>, new()
        where TUpdate: BaseDictionaryUpdateCommand<TIdto, TOdto>, new()
        where TDelete: BaseDictionaryDeleteCommand, new()
    {
        private readonly ISender _sender;
        public BaseDictionaryController(ISender sender)
        {
            _sender = sender;
        }


        [HttpGet]
        public async Task<ApiResponse<IEnumerable<TOdto>>> GetList([FromQuery] BaseDictionaryFilter filter)
        {
            
            var result = await _sender.Send(new TGetList {Filter = filter});
            return ResponseOk(result);
        }
        
        [HttpGet("{id:guid}")]
        public async Task<ApiResponse<TOdto>> Get(Guid id)
        {
            var result = await _sender.Send(new TGetById {Id = id});
            return ResponseOk(result);
        }

        [HttpGet(nameof(CheckExistenceCode))]
        public async Task<ApiResponse<bool>> CheckExistenceCode(string code)
        {
            var result = await _sender.Send(new TCheckExistenceCode {Code = code});
            return ResponseOk(result);
        }
        
        [HasPermission(PermissionEnum.Admin)]
        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.Created)]
        public async Task<ApiResponse<TOdto>> Post(TIdto idto)
        {
            var result = await _sender.Send(new TCreate{Idto = idto});
            return ResponseCreated(result);
        }
        
        [HasPermission(PermissionEnum.Admin)]
        [HttpPut("{id:guid}")]
        [ProducesResponseType((int)HttpStatusCode.NoContent)]
        public async Task<ApiResponse> Put(Guid id, TIdto idto)
        {
            var result = await _sender.Send(new TUpdate{ Id = id, Idto = idto});
            return ResponseNoContent();
        }
        
        [HasPermission(PermissionEnum.Admin)]
        [HttpDelete("{id:guid}")]
        [ProducesResponseType((int)HttpStatusCode.NoContent)]
        public async Task<ApiResponse> Delete(Guid id)
        {
            var result = await _sender.Send(new TDelete{Id = id});
            return ResponseNoContent();
        }
    }
}