using System.Text.Json.Serialization;
using ApplicationService.Implementation;
using DataAccess.PostgreSql;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Web.UseCases;
using WebApi.Swagger;
using Serilog;
using Utils;
using Utils.Settings;
using WebApi.Configurations;
using WebApi.Services;

namespace WebApi
{
    public class Startup
    {
        private const string DeveloperOrigins = "_developerOrigins";
        private const string ProductionOrigins = "_productionOrigins";
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            Log.Logger = new LoggerConfiguration().ReadFrom.Configuration(Configuration)
                .CreateLogger();
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //HangFireConfig.Register(services, Configuration);
            services.AddCors(options => options.AddDefaultPolicy(builder => builder
                    .SetIsOriginAllowed((host) => true)
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                )
            );
            services.AddControllers();
            services.AddHttpContextAccessor();
            services.RegisterDb(Configuration);
            OAuthConfig.Register(services, Configuration);
            services.ConfigureSwagger();
            
            services.AddMvc(config => { config.Filters.Add(new ModelStateCheckFilter()); }).AddJsonOptions(options =>
            {
                options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
            });
            services.RegisterApplicationService();
            services.ConfigureSettings(Configuration);
            
            // services.AddCors(options => options.AddPolicy(ProductionOrigins, builder => builder
            //     .WithOrigins("https://hr.kmg.kz")
            //     .AllowAnyMethod()
            //     .AllowAnyHeader()
            //     )
            // );
            services.RegistrationServices(Configuration);
            services.RegisterWebUseCases();
            
            
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            
            app.UseSwaggerDocumentation();
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseHttpsRedirection();
            
            app.UseMiddleware<ApplicationIdMiddleware>();
            OAuthConfig.RegisterApp(app);
            
            
            app.UseRouting();
            app.UseCors();
            
            
            app.UseAuthorization();
            app.UseStaticFiles(new StaticFileOptions()
            {
                OnPrepareResponse = (context) =>
                {
                    if (!context.Context.User.Identity.IsAuthenticated && context.Context.Request.Path.StartsWithSegments("/Files"))
                    {
                        throw ApiException.Unauthorized();
                    }
                }
            });
            
            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }
    }
}