﻿using AutoMapper;
using DataAccess.Interfaces;
using Entities.Models.Dictionaries;
using Web.UseCases.Dictionaries.BaseDictionaries.Commands;
using Web.UseCases.Dictionaries.Localizations.Dto;

namespace Web.UseCases.Dictionaries.Localizations.Commands
{
    public class LocalizationCreateCommand: BaseDictionaryCreateCommand<LocalizationIdto, LocalizationOdto>
    {
        public class Handler : Handler<LocalizationCreateCommand,Localization, LocalizationIdto, LocalizationOdto>
        {
            public Handler(IMapper mapper, IDbContext dbContext): base(mapper, dbContext)
            {
                
            }
        }
    }
}