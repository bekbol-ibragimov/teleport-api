﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Utils.Pagination
{
    public class PaginationRequest<TSortField>: BaseSortFilter<TSortField> where TSortField: struct, Enum
    {
        /// <summary>
        /// Номер страницы
        /// </summary>
        [Range(1, int.MaxValue)]
        public int PageNumber { get; set; } = 1;

        /// <summary>
        /// Кол-во строк на странице
        /// </summary>
        [Range(1, int.MaxValue)]
        public int PageSize { get; set; } = 100;

    }
}