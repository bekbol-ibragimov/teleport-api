﻿using System.ComponentModel.DataAnnotations;
using Entities.Models.Global;

namespace Web.UseCases.Dictionaries.BaseDictionaries.Dto
{
    public abstract class BaseDictionaryIdto
    {
        public string Code { get; set; }
        [Required] 
        public string NameRu { get; set; }
        public string NameKz { get; set; }
        public string NameEn { get; set; }
    }
}