﻿namespace Web.UseCases.Dictionaries.Countries.Dto
{
    public class CountryDto
    {
        public string NameRu { get; set; }
    }
}