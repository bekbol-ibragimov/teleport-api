﻿using System;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Utils.Pagination;
using Web.Controllers.Common;
using Web.UseCases.Auth.User.Commands;
using Web.UseCases.Auth.User.Dto;
using Web.UseCases.Auth.User.Queries;
using Web.UseCases.Wrappers;

namespace Web.Controllers.Auth
{
    public class UserController: BaseController
    {
        private readonly ISender _sender;

        public UserController(ISender sender)
        {
            _sender = sender;
        }

        [HttpPost]
        public async Task<ApiResponse<Guid>> CreateUser([FromBody] UserCreateDto request)
        {
            return ResponseOk(await _sender.Send(new UserCreateCommand {Dto = request}));
        }
        
        [HttpPut("{id:guid}")]
        public async Task<ApiResponse> UpdateUser(Guid id, [FromBody] UserUpdateDto request)
        {
            await _sender.Send(new UserUpdateCommand {Id = id, Dto = request});
            return ResponseOk();
        }
        
        [HttpGet]
        public async Task<ApiResponse<PaginationResponse<UserDto>>> GetList([FromQuery] UserFilterDto request)
        {
            var ss = await _sender.Send(new UserGetListQuery {Filter = request});
            return ResponseOk(ss);
        }
    }
}