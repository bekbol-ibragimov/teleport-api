﻿using System;

namespace Web.UseCases.Auth.Account.Dto
{
    public class AuthDto
    {
        public Guid UserId { get; set; }
        public string UserName { get; set; }
        public string FullName { get; set; }
        public string Token { get; set; }
        public DateTime ExpiredDate { get; set; }
        public string PhotoPath { get; set; }
        public string[] Permissions { get; set; }
    }
}