﻿using AutoMapper;
using DataAccess.Interfaces;
using Entities.Models.Dictionaries;
using Web.UseCases.Dictionaries.BaseDictionaries.Queries;
using Web.UseCases.Dictionaries.Localizations.Dto;

namespace Web.UseCases.Dictionaries.Localizations.Queries
{
    public class LocalizationGetByIdQuery: BaseDictionaryGetByIdQuery<LocalizationOdto>
    {
        public class Handler : Handler<LocalizationGetByIdQuery,Localization, LocalizationOdto>
        {
            public Handler(IMapper mapper, IDbContext dbContext): base(mapper, dbContext)
            {
                
            }
        }
    }
}