﻿using System;
using System.IO;
using System.Threading.Tasks;
using ApplicationService.Interfaces.Enums;
using ApplicationService.Interfaces.Files;
using DataAccess.Interfaces;
using Entities.Models.Files;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Utils;
using WebApp.Interfaces.Services;

namespace ApplicationService.Implementation.Files
{
    public class FileTempService: IFileTempService
    {
        private readonly IHttpContextService _httpContext;
        private readonly IDbContext _dbContext;
        private readonly string _rootPath;

        public FileTempService(IHttpContextService httpContext, IDbContext dbContext, IWebHostEnvironment environment)
        {
            _httpContext = httpContext;
            _dbContext = dbContext;
            _rootPath = environment.WebRootPath;
        }
        
        public async Task<string> CopyToPath( Guid fileId, FilePath newPath)
        {
            var entity = await _dbContext.FileTemps
                .FirstOrDefaultAsync(x => x.CreatedUserId == _httpContext.UserId && x.Id == fileId);
            if(entity == null)
                throw ApiException.NotFound(nameof(FileTemp));
            
            var newFilePath = $"/{FilePath.Files.ToString()}/{newPath.ToString()}/{entity.FileName}";
            var oldFullPath = $"{_rootPath}{entity.FullPath}";
            var newFullPath = $"{_rootPath}{newFilePath}";
            ValidatePath(newFullPath);
            File.Copy(oldFullPath, newFullPath, true);
            
            return newFilePath;
        }
        
        private void ValidatePath(string filePath)
        {
            var dirPath = Path.GetDirectoryName(filePath);
            if (!Directory.Exists(dirPath))
            {
                Directory.CreateDirectory(dirPath);
            }
        }
    }
}