﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using DataAccess.Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Utils.Pagination;
using Web.UseCases.Auth.User.Dto;
using Web.UseCases.Base;
using WebApp.Interfaces.Services;

namespace Web.UseCases.Auth.User.Queries
{
    public class UserGetListQuery: IRequest<PaginationResponse<UserDto> >
    {
        public UserFilterDto Filter { get; set; }
        
        public class Handler: BaseHandler<UserGetListQuery, PaginationResponse<UserDto>>
        {
            public Handler(IMapper mapper, IDbContext dbContext, IHttpContextService httpContextService) : base(mapper, dbContext, httpContextService)
            {
            }

            public override async Task<PaginationResponse<UserDto>> Handle(UserGetListQuery request, CancellationToken cancellationToken)
            {
                var filter = request.Filter;
                return await _dbContext.AuthUsers.Where(x => x.IsDeleted == false)
                    .Include(c => c.Company)
                    .Where(x => filter.LastName == null || filter.LastName.Contains(x.LastName))
                    .AsNoTracking()
                    .OrderBy(filter)
                    .ProjectTo<UserDto>(_mapper.ConfigurationProvider)
                    .ToPaginationListAsync(filter);
            }
        }
    }
}