﻿using System;
using Entities.Enums;
using Entities.Models.Auth;
using Entities.Models.Global;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.PostgreSql.Seeds
{
    public static class UserSeed
    {
        public static void AddPermissions(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Permission>().HasData(
                new Permission()
                {
                    Id = new Guid("3e3e1deb-8411-442a-8a54-3975a3de9804"),
                    Code = PermissionEnum.Sa,
                    NameRu = "Суперадмин",
                    NameKz = "Суперадмин",
                    NameEn = "Суперадмин",
                    CreatedAt = new DateTime(),
                    UpdatedAt = new DateTime()
                },
                new Permission()
                {
                    Id = new Guid("5cc42b53-2bb0-4b8b-91a4-67ab5fe4f3c7"),
                    Code = PermissionEnum.Admin,
                    NameRu = "Администратор",
                    NameKz = "Администратор",
                    NameEn = "Администратор",
                    CreatedAt = new DateTime(),
                    UpdatedAt = new DateTime()
                },
                new Permission()
                {
                    Id = new Guid("69c131f9-3b21-463e-992e-e5aa508172a2"),
                    Code = PermissionEnum.User,
                    NameRu = "Пользователь",
                    NameKz = "Пользователь",
                    NameEn = "Пользователь",
                    CreatedAt = new DateTime(),
                    UpdatedAt = new DateTime()
                }
            );
        }
    }
}