﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace Utils
{
    public static class HashUtils
    {
        public static string DoHash(string value)
        {
            using (var alg = SHA512.Create())
            {
                var hash = alg.ComputeHash(Encoding.UTF8.GetBytes(value));
                return BitConverter.ToString(hash).Replace("-", string.Empty).ToLower();
            };
        }


    }
}