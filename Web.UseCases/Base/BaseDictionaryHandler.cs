﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using DataAccess.Interfaces;
using Entities.Models.Global;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Web.UseCases.Base
{
    public abstract class BaseDictionaryHandler<TRequest, TResponse, TEntity> : IRequestHandler<TRequest, TResponse>
        where TRequest : IRequest<TResponse>
        where TEntity : BaseEntity 
    {
        private readonly IDbContext _dbContext;
        protected readonly DbSet<TEntity> Repo;
        private readonly IMapper _map;

        protected BaseDictionaryHandler(IMapper mapper, IDbContext dbContext)
        {
            _dbContext = dbContext;
            _map = mapper;
            Repo = dbContext.Set<TEntity>();
        }
        
        public abstract Task<TResponse> Handle(TRequest request, CancellationToken cancellationToken);
        
    }
}