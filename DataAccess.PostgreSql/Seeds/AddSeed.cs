﻿using Microsoft.EntityFrameworkCore;

namespace DataAccess.PostgreSql.Seeds
{
    public static class AddSeed
    {
        public static void AddSeeds(this ModelBuilder modelBuilder)
        {
            modelBuilder.AddPermissions();
            modelBuilder.AddDefaultUsers();
        }
    }
}