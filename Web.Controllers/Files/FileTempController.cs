﻿using System;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Utils;
using Utils.Extensions;
using Web.Controllers.Common;
using Web.UseCases.Files.FileTemps.Command;
using Web.UseCases.Files.FileTemps.Dto;
using Web.UseCases.Files.FileTemps.Queries;
using Web.UseCases.Wrappers;

namespace Web.Controllers.Files
{
    public class FileTempController: BaseController
    {
        private readonly ISender _sender;
        public FileTempController(ISender sender)
        {
            _sender = sender;
        }
        
        [HttpGet("{id:guid}")]
        public async Task<FileContentResult> Get(Guid id)
        {
            var data = await _sender.Send(new FileTempGetByIdQuery{Id = id});

            FileContentResult result = File(data.File, "application/ms-excel", data.Name);
            return result;
        }

        [HttpPost]
        [ProducesResponseType(201)]
        public async Task<ApiResponse<FileTempOdto>> Post([FromForm] IFormFile[] file)
        {
            if (file.IsNullOrEmpty())
                throw ApiException.BadRequest("file not found");
            var result = await _sender.Send(new FileTempCreateCommand() {File = file[0]});
            return ResponseCreated(result);
        }
    }
}