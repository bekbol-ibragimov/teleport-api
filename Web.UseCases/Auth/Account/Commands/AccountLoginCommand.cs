﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DataAccess.Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Utils;
using Web.UseCases.Auth.Account.Dto;

namespace Web.UseCases.Auth.Account.Commands
{
    public class AccountLoginCommand : IRequest<AuthDto>
    {
        public LoginDto Dto { get; set; }
        
        public class Handler : AccountBaseHandler<AccountLoginCommand, AuthDto>
        {
            private readonly IDbContext _dbContext;

            public Handler(IDbContext dbContext, IConfiguration config) 
                : base(config, dbContext)
            {
                _dbContext = dbContext;
            }

            public override async Task<AuthDto> Handle(AccountLoginCommand request, CancellationToken cancellationToken)
            {
                var user = await _dbContext.AuthUsers
                    .Where(x => x.IsActive)
                    .Where(x => x.UserName.ToLower() == request.Dto.UserName.ToLower() 
                                || x.Iin.ToLower() == request.Dto.UserName.ToLower() 
                                || x.Email.ToLower() == request.Dto.UserName.ToLower())
                    .FirstOrDefaultAsync(cancellationToken: cancellationToken);
            
                if (user == null || HashUtils.DoHash(request.Dto.Password) != user.PasswordHash)  
                    throw ApiException.BadRequest("usernameOrPasswordIncorrect");
        
        
                return await GenerateJwToken(user);
            }

        }
    }
}