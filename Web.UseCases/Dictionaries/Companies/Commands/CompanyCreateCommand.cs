﻿using AutoMapper;
using DataAccess.Interfaces;
using Entities.Models.Dictionaries;
using Web.UseCases.Dictionaries.BaseDictionaries.Commands;
using Web.UseCases.Dictionaries.Companies.Dto;

namespace Web.UseCases.Dictionaries.Companies.Commands
{
    public class CompanyCreateCommand: BaseDictionaryCreateCommand<CompanyIdto, CompanyOdto>
    {
        public class Handler : Handler<CompanyCreateCommand,Company, CompanyIdto, CompanyOdto>
        {
            public Handler(IMapper mapper, IDbContext dbContext): base(mapper, dbContext)
            {
                
            }
        }
    }
}