﻿using System;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using DataAccess.Interfaces;
using Entities.Models.Files;
using GeoCoordinatePortable;
using MediatR;
using Microsoft.AspNetCore.Hosting;
using Utils;
using Web.UseCases.Airports.Dto;
using Web.UseCases.Base;
using Web.UseCases.Files.FileTemps.Dto;
using WebApp.Interfaces.Services;

namespace Web.UseCases.Airports.Queries
{
    public class AirportGetDistance : IRequest<DistanceOdto>
    {
        public string Airport1 { get; set; }
        public string Airport2 { get; set; }
        

        public class Handler : IRequestHandler<AirportGetDistance, DistanceOdto>
        {
            private readonly ITeleportClient _teleportClient;

            public Handler(ITeleportClient teleportClient)
            {
                _teleportClient = teleportClient;
            }

            public async Task<DistanceOdto> Handle(AirportGetDistance request, CancellationToken cancellationToken)
            {
                var airport1 = await _teleportClient.SendAsync<AirportDto>(request.Airport1);
                var airport2 = await _teleportClient.SendAsync<AirportDto>(request.Airport2);
                
                var coord1 = new GeoCoordinate(airport1.Location.Lat, airport1.Location.Lon);
                var coord2 = new GeoCoordinate(airport2.Location.Lat, airport2.Location.Lon);

                var meter = coord1.GetDistanceTo(coord2);
                var mile = meter * 0.000621371;
                return new DistanceOdto{Meter = meter, Mile = mile};
            }
            

        }
    }
}