using System;
using System.Threading.Tasks;
using Entities.Models.Global;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Web.Controllers.Common
{
    public class BaseDictionaryController<TService, TEntity, TDto> : BaseController
        where TEntity : BaseDictionaryEntity
        where TDto : class
    {
        // public BaseDictionaryController(TService service, IHttpContextAccessor context)
        //     : base(service, context)
        // {
        // }
        //
        // [HttpGet]
        // public virtual IActionResult Get([FromQuery]BasePageableQuery query)
        // {
        //     var data = Service.PagedListAsync(x => !x.IsDeleted, query);
        //     return ResponseOk(data);
        // }
        //
        // [HttpGet("list")]
        // public virtual async Task<IActionResult> GetList()
        // {
        //     var data = await Service.ListAsync(x => !x.IsDeleted);
        //     return ResponseOk(data);
        // }
        //
        // [HttpGet("{id}")]
        // public virtual async Task<IActionResult> GetById(Guid id)
        // {
        //     return Ok(await Service.GetById(id));
        // }
    }
}