using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Web.Controllers.Common;
using Web.UseCases.Auth.Account.Commands;
using Web.UseCases.Auth.Account.Dto;
using Web.UseCases.Wrappers;

namespace Web.Controllers.Auth
{
    public class AccountController : BaseController
    {
        private readonly ISender _sender;
        public AccountController(ISender sender)
        {
            _sender = sender;
        }

        /// <summary>
        /// Аутентификация пользователя
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost("authenticate")]
        public async Task<ApiResponse<AuthDto>> Auth([FromBody]LoginDto request)
        {
            return ResponseOk(await _sender.Send(new AccountLoginCommand {Dto = request}));
        }
        
        [AllowAnonymous]
        [HttpGet("sign-raw")]
        public async Task<ApiResponse<string>> SingRaw()
        {
            return ResponseOk(await _sender.Send(new AccountSignRawCommand {Ip = GenerateIpAddress()}) );
        }
        
        /// <summary>
        /// Аутентификация пользователя через ЭЦП
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost("authenticate/sign")]
        public async Task<ApiResponse<AuthDto>> AuthSign([FromBody]LoginSignDto request)
        {
            return ResponseOk(await _sender.Send(new AccountLoginSignCommand {Dto = request, Ip = GenerateIpAddress()}));
        }
        
        // /// <summary>
        // /// Получение текущего пользователя
        // /// </summary>
        // /// <returns></returns>
        // [HttpGet("current")]
        // public async Task<IActionResult> CurrentUser()
        // {
        //     return ResponseOk(await Service.GetCurrentUserAsync());
        // }

        private string GenerateIpAddress()
        {
            if (Request.Headers.ContainsKey("X-Forwarded-For"))
                return Request.Headers["X-Forwarded-For"];
            return HttpContext.Connection.RemoteIpAddress.MapToIPv4().ToString();
        }

    }
}