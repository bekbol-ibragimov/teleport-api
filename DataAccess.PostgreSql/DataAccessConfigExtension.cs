﻿using System;
using DataAccess.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace DataAccess.PostgreSql
{
    public static class DataAccessConfigExtension
    {
        public static void RegisterDb(this IServiceCollection services, IConfiguration configuration)
        {
            if (configuration.GetValue<bool>("UseInMemoryDatabase"))
            {
                services.AddDbContext<IDbContext, AppDbContext>(options =>
                    options.UseInMemoryDatabase("ApplicationDb"));
            }
            else
            {
                var connection = configuration.GetConnectionString("DefaultConnection");
                Console.WriteLine("DefaultConnection: {0}", connection);
                services.AddDbContext<IDbContext, AppDbContext>(options =>
                    options.UseNpgsql(
                        configuration.GetConnectionString("DefaultConnection"),
                        b => b.MigrationsAssembly(typeof(AppDbContext).Assembly.FullName)));
            }
        }
    }
}