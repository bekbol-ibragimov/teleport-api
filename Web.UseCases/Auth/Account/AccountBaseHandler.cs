﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using DataAccess.Interfaces;
using Entities.Models.Auth;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Web.UseCases.Auth.Account.Dto;
using Web.UseCases.Enums;

namespace Web.UseCases.Auth.Account
{
    public abstract class AccountBaseHandler<TRequest, TResponse> : IRequestHandler<TRequest, TResponse> 
        where TRequest : IRequest<TResponse>
    {
        private readonly IConfiguration _config;
        private readonly IDbContext _dbContext;

        protected AccountBaseHandler(IConfiguration config, IDbContext dbContext)
        {
            _config = config;
            _dbContext = dbContext;
        }

        public abstract Task<TResponse> Handle(TRequest request, CancellationToken cancellationToken);


        protected async Task<AuthDto> GenerateJwToken(Entities.Models.Auth.User user)
        {
            var permissionCodes = await _dbContext.AuthPermissions
                .Join(_dbContext.AuthUserPermissions, role => role.Id, userRole => userRole.PermissionId, 
                    (role, userPermission) => new {role, userPermission})
                .Where(x => x.userPermission.UserId == user.Id)
                .Select(x => x.role.Code.ToString())
                .ToListAsync();
        
            var claims = new List<Claim>();
            AddClaims(claims, user, permissionCodes.ToArray());
            var seconds = TokenExpiredSeconds();
            
            var authDto = new AuthDto()
            {
                UserName = user.UserName,
                ExpiredDate = DateTime.Now.AddSeconds(seconds),
                UserId = user.Id,
                FullName = user.FullName,
                PhotoPath = user.PhotoPath,
                Token = GenerateToken(claims.ToArray(), seconds),
                Permissions = permissionCodes.ToArray()
            };
        
            return authDto;
        }

        private void AddClaims(List<Claim> claims, Entities.Models.Auth.User user, string[] permissions)
        {
            claims.Add(new Claim(ClaimType.UserId, user.Id.ToString()));
            claims.Add(new Claim(ClaimType.Username, user.UserName));
            claims.Add(new Claim(ClaimType.UserEmail, user.Email));
            claims.AddRange(permissions.Select(permission => new Claim(ClaimType.Permission, permission)));
        }

        private string GenerateToken(Claim[] claims, int seconds) 
        {
            var ss = DateTime.Now.AddSeconds(seconds);
            var token = new JwtSecurityToken(
                issuer: _config["Jwt:Key"],
                audience: _config["Jwt:Audience"],
                claims: claims,
                expires: ss,
                signingCredentials: new SigningCredentials(
                    new SymmetricSecurityKey(Encoding.ASCII.GetBytes(_config["Jwt:Key"])),
                    SecurityAlgorithms.HmacSha256)
            );
            var jwtToken = new JwtSecurityTokenHandler().WriteToken(token);
            return jwtToken;
        }

        private int TokenExpiredSeconds()
        {
            string minuteStr = _config["Jwt:AccessTokenExpireMinute"];
            if (!int.TryParse(minuteStr, out var minute))
                minute = (int) TimeSpan.FromMinutes(30).TotalSeconds;
            return (int) TimeSpan.FromMinutes(minute).TotalSeconds;
        }
    }
}