﻿using Newtonsoft.Json;

namespace ApplicationService.Interfaces.Auth.Dto.Sign.Request
{
    public class SignCheckParamDto
    {
        [JsonProperty("checkCrl")] public bool VerifyCrl { get; set; }
        [JsonProperty("checkOcsp")] public bool VerifyOcsp { get; set; }

        /// <summary>
        /// Base64
        /// </summary>
        [JsonProperty("cms")]
        public string Cms { get; set; }
    }
}