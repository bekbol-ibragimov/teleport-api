﻿using System;
using System.ComponentModel.DataAnnotations;
using Entities.Enums;

namespace Web.UseCases.Auth.User.Dto
{
    public class UserCreateDto
    {
        public string Password { get; set; }
        [MaxLength(50)]
        public string UserName { get; set; }
        [MaxLength(12)]
        public string Iin { get; set; }
        [MaxLength(200)]
        public string Email { get; set; }
        [MaxLength(200)]
        public string LastName { get; set; }
        [MaxLength(200)]
        public string FirstName { get; set; }
        [MaxLength(200)]
        public string MiddleName { get; set; }
        /// <summary>
        /// Дата рождения
        /// </summary>
        public DateTime? BirthDate { get; set; }
        /// <summary>
        /// Пол
        /// </summary>
        public GenderEnum? Sex { get; set; }
        
        /// <summary>
        /// Ссылка на фото
        /// </summary>
        public string PhotoPath { get; set; }
        /// <summary>
        /// Участник кадрового резерва
        /// </summary>
        [Required]
        public bool IsEmployeePool { get; set; } = false;
        /// <summary>
        /// Если участник кадрового резерва то заполняется 
        /// </summary>
        public PoolEnum? Pool { get; set; }
        [Required]
        public Guid? CompanyId { get; set; }
    }
}