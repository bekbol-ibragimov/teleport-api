﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Entities.Models.Global;

namespace Entities.Models.Auth
{
    public class RolePermission: BaseEntity
    {
        public Guid RoleId { get; set; }
        public Guid PermissionId { get; set; }
        
        
        
        
        //--------------------------------------------------------------------------------------------------------------
        
        [ForeignKey(nameof(RoleId))]
        public Role Role { get; set; }
        
        [ForeignKey(nameof(PermissionId))]
        public Permission Permission { get; set; }
    }
}