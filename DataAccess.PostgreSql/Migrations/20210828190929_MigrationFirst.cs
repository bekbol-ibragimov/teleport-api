﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DataAccess.PostgreSql.Migrations
{
    public partial class MigrationFirst : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AuthPermissions",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: false),
                    CreatedUserId = table.Column<Guid>(nullable: false),
                    UpdatedUserId = table.Column<Guid>(nullable: false),
                    Code = table.Column<string>(nullable: false),
                    NameRu = table.Column<string>(nullable: false),
                    NameKz = table.Column<string>(nullable: true),
                    NameEn = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AuthPermissions", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AuthRoles",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: false),
                    CreatedUserId = table.Column<Guid>(nullable: false),
                    UpdatedUserId = table.Column<Guid>(nullable: false),
                    NameRu = table.Column<string>(nullable: false),
                    NameKz = table.Column<string>(nullable: true),
                    NameEn = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AuthRoles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DicCompanies",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: false),
                    CreatedUserId = table.Column<Guid>(nullable: false),
                    UpdatedUserId = table.Column<Guid>(nullable: false),
                    Code = table.Column<string>(nullable: false),
                    NameRu = table.Column<string>(nullable: false),
                    NameKz = table.Column<string>(nullable: true),
                    NameEn = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DicCompanies", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DicCountries",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: false),
                    CreatedUserId = table.Column<Guid>(nullable: false),
                    UpdatedUserId = table.Column<Guid>(nullable: false),
                    Code = table.Column<string>(nullable: false),
                    NameRu = table.Column<string>(nullable: false),
                    NameKz = table.Column<string>(nullable: true),
                    NameEn = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DicCountries", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DicLocalizations",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: false),
                    CreatedUserId = table.Column<Guid>(nullable: false),
                    UpdatedUserId = table.Column<Guid>(nullable: false),
                    Code = table.Column<string>(nullable: false),
                    NameRu = table.Column<string>(nullable: false),
                    NameKz = table.Column<string>(nullable: true),
                    NameEn = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DicLocalizations", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "FileTemps",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: false),
                    CreatedUserId = table.Column<Guid>(nullable: false),
                    UpdatedUserId = table.Column<Guid>(nullable: false),
                    FullPath = table.Column<string>(nullable: true),
                    FileName = table.Column<string>(nullable: true),
                    OriginalFileName = table.Column<string>(nullable: true),
                    Extension = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FileTemps", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AuthRolePermissions",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: false),
                    CreatedUserId = table.Column<Guid>(nullable: false),
                    UpdatedUserId = table.Column<Guid>(nullable: false),
                    RoleId = table.Column<Guid>(nullable: false),
                    PermissionId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AuthRolePermissions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AuthRolePermissions_AuthPermissions_PermissionId",
                        column: x => x.PermissionId,
                        principalTable: "AuthPermissions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AuthRolePermissions_AuthRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AuthRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AuthUsers",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: false),
                    CreatedUserId = table.Column<Guid>(nullable: false),
                    UpdatedUserId = table.Column<Guid>(nullable: false),
                    PasswordHash = table.Column<string>(nullable: true),
                    UserName = table.Column<string>(maxLength: 50, nullable: true),
                    Iin = table.Column<string>(maxLength: 200, nullable: true),
                    Email = table.Column<string>(maxLength: 200, nullable: true),
                    LastName = table.Column<string>(maxLength: 200, nullable: true),
                    FirstName = table.Column<string>(maxLength: 200, nullable: true),
                    MiddleName = table.Column<string>(maxLength: 200, nullable: true),
                    BirthDate = table.Column<DateTime>(nullable: true),
                    Sex = table.Column<int>(nullable: true),
                    PhotoPath = table.Column<string>(nullable: true),
                    IsActive = table.Column<bool>(nullable: false),
                    IsEmployeePool = table.Column<bool>(nullable: false),
                    Pool = table.Column<int>(nullable: true),
                    CompanyId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AuthUsers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AuthUsers_DicCompanies_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "DicCompanies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AuthUserPermissions",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: false),
                    CreatedUserId = table.Column<Guid>(nullable: false),
                    UpdatedUserId = table.Column<Guid>(nullable: false),
                    UserId = table.Column<Guid>(nullable: false),
                    PermissionId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AuthUserPermissions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AuthUserPermissions_AuthPermissions_PermissionId",
                        column: x => x.PermissionId,
                        principalTable: "AuthPermissions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AuthUserPermissions_AuthUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AuthUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AuthUserRoles",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: false),
                    CreatedUserId = table.Column<Guid>(nullable: false),
                    UpdatedUserId = table.Column<Guid>(nullable: false),
                    UserId = table.Column<Guid>(nullable: false),
                    RoleId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AuthUserRoles", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AuthUserRoles_AuthRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AuthRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AuthUserRoles_AuthUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AuthUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                table: "AuthPermissions",
                columns: new[] { "Id", "Code", "CreatedAt", "CreatedUserId", "IsDeleted", "NameEn", "NameKz", "NameRu", "UpdatedAt", "UpdatedUserId" },
                values: new object[,]
                {
                    { new Guid("3e3e1deb-8411-442a-8a54-3975a3de9804"), "Sa", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new Guid("00000000-0000-0000-0000-000000000000"), false, "Суперадмин", "Суперадмин", "Суперадмин", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new Guid("00000000-0000-0000-0000-000000000000") },
                    { new Guid("5cc42b53-2bb0-4b8b-91a4-67ab5fe4f3c7"), "Admin", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new Guid("00000000-0000-0000-0000-000000000000"), false, "Администратор", "Администратор", "Администратор", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new Guid("00000000-0000-0000-0000-000000000000") },
                    { new Guid("69c131f9-3b21-463e-992e-e5aa508172a2"), "User", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new Guid("00000000-0000-0000-0000-000000000000"), false, "Пользователь", "Пользователь", "Пользователь", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new Guid("00000000-0000-0000-0000-000000000000") }
                });

            migrationBuilder.InsertData(
                table: "AuthUsers",
                columns: new[] { "Id", "BirthDate", "CompanyId", "CreatedAt", "CreatedUserId", "Email", "FirstName", "Iin", "IsActive", "IsDeleted", "IsEmployeePool", "LastName", "MiddleName", "PasswordHash", "PhotoPath", "Pool", "Sex", "UpdatedAt", "UpdatedUserId", "UserName" },
                values: new object[] { new Guid("b13ba310-4f4e-4984-9ce9-7879d767d5b4"), null, null, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new Guid("00000000-0000-0000-0000-000000000000"), "sa@demo.kz", "Са", null, true, false, false, "Саов", "Саович", "1e1bd7b4a2d36a821011acfa43aa7625e18ad0a76d5193b70cf43d1f6753b4da53c0b015a63d4d0054cfb0cfc38008a3bcda476af402b55ba10b3bf3667dac2a", null, null, null, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new Guid("00000000-0000-0000-0000-000000000000"), "sa" });

            migrationBuilder.InsertData(
                table: "AuthUserPermissions",
                columns: new[] { "Id", "CreatedAt", "CreatedUserId", "IsDeleted", "PermissionId", "UpdatedAt", "UpdatedUserId", "UserId" },
                values: new object[] { new Guid("5aee0fa6-0ee8-4327-a821-f98d5245792e"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new Guid("00000000-0000-0000-0000-000000000000"), false, new Guid("3e3e1deb-8411-442a-8a54-3975a3de9804"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new Guid("00000000-0000-0000-0000-000000000000"), new Guid("b13ba310-4f4e-4984-9ce9-7879d767d5b4") });

            migrationBuilder.CreateIndex(
                name: "IX_AuthRolePermissions_PermissionId",
                table: "AuthRolePermissions",
                column: "PermissionId");

            migrationBuilder.CreateIndex(
                name: "IX_AuthRolePermissions_RoleId",
                table: "AuthRolePermissions",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_AuthUserPermissions_PermissionId",
                table: "AuthUserPermissions",
                column: "PermissionId");

            migrationBuilder.CreateIndex(
                name: "IX_AuthUserPermissions_UserId",
                table: "AuthUserPermissions",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AuthUserRoles_RoleId",
                table: "AuthUserRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_AuthUserRoles_UserId",
                table: "AuthUserRoles",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AuthUsers_CompanyId",
                table: "AuthUsers",
                column: "CompanyId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AuthRolePermissions");

            migrationBuilder.DropTable(
                name: "AuthUserPermissions");

            migrationBuilder.DropTable(
                name: "AuthUserRoles");

            migrationBuilder.DropTable(
                name: "DicCountries");

            migrationBuilder.DropTable(
                name: "DicLocalizations");

            migrationBuilder.DropTable(
                name: "FileTemps");

            migrationBuilder.DropTable(
                name: "AuthPermissions");

            migrationBuilder.DropTable(
                name: "AuthRoles");

            migrationBuilder.DropTable(
                name: "AuthUsers");

            migrationBuilder.DropTable(
                name: "DicCompanies");
        }
    }
}
