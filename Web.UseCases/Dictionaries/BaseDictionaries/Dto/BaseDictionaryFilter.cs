﻿using Utils.Pagination;

namespace Web.UseCases.Dictionaries.BaseDictionaries.Dto
{
    public class BaseDictionaryFilter: BaseSortFilter<BaseDictionaryOrderBy>
    {
        public string SearchString { get; set; }
    }
    
    public enum BaseDictionaryOrderBy
    {
        NameRu,
        NameKz,
        NameEn,
        Code,
        CreatedAt,
        UpdatedAt
    }
}