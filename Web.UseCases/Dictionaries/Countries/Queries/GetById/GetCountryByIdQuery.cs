﻿using System;
using MediatR;
using Web.UseCases.Dictionaries.Countries.Dto;

namespace Web.UseCases.Dictionaries.Countries.Queries.GetById
{
    public class GetCountryByIdQuery : IRequest<CountryDto>
    {
        public Guid Id { get; set; }
    }
}