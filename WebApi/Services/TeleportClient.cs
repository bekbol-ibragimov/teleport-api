﻿using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Utils;
using Utils.Extensions;
using Utils.Settings;
using WebApp.Interfaces.Services;

namespace WebApi.Services
{
    public class TeleportClient: ITeleportClient
    {
        private readonly AppSettings _settings;
        private readonly HttpClient _httpClient;
        private readonly ILogger<TeleportClient> _logger;

        public TeleportClient(AppSettings settings, HttpClient httpClient, ILogger<TeleportClient> logger)
        {
            _settings = settings;
            _httpClient = httpClient;
            _logger = logger;
        }

        public async Task<TResponse> SendAsync<TResponse>(string airport)
            where TResponse: class
        {
            var url = $"{_settings.Teleport.Url}/airports/{airport}";
            HttpResponseMessage response = await _httpClient.GetAsync(url);

            var content = await response.Content.ReadAsStringAsync();

            if (response.IsSuccessStatusCode) 
                return JsonConvert.DeserializeObject<TResponse>(content);
            
            if (response.StatusCode == HttpStatusCode.NotFound) 
                throw ApiException.NotFound($"airport {airport}");

            _logger.LogError($"GET:{url}\nresponse:{content}");
            throw new Exception("error TeleportClient");
        }
    }
}