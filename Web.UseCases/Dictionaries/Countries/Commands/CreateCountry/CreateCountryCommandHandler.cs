﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using DataAccess.Interfaces;
using Entities.Models.Dictionaries;
using MediatR;

namespace Web.UseCases.Dictionaries.Countries.Commands.CreateCountry
{
    public class CreateCountryCommandHandler : IRequestHandler<CreateCountryCommand, Guid>
    {
        private readonly IMapper _mapper;
        private readonly IDbContext _dbContext;

        public CreateCountryCommandHandler(IMapper mapper, IDbContext dbContext)
        {
            _mapper = mapper;
            _dbContext = dbContext;
        }

        public async Task<Guid> Handle(CreateCountryCommand request, CancellationToken cancellationToken)
        {
            var country = _mapper.Map<Entities.Models.Dictionaries.Country>(request.Dto);
            _dbContext.DicCountries.Add(country);
            await _dbContext.SaveChangesAsync(cancellationToken);
            return country.Id;
        }
    }
}