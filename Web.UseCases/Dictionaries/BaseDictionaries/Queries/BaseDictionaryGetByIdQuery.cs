﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using DataAccess.Interfaces;
using Entities.Models.Global;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Web.UseCases.Base;
using Web.UseCases.Dictionaries.BaseDictionaries.Dto;

namespace Web.UseCases.Dictionaries.BaseDictionaries.Queries
{
    public abstract class BaseDictionaryGetByIdQuery<TOdto1> : IRequest<TOdto1>
        where TOdto1 : BaseDictionaryOdto
    {
        public Guid Id { get; set; }
        
        public class Handler<TRequest, TEntity, TOdto> : BaseDictionaryHandler<TRequest, TOdto, TEntity>
            where TRequest: BaseDictionaryGetByIdQuery<TOdto>
            where TEntity : BaseDictionaryEntity
            where TOdto : BaseDictionaryOdto
        {
            private readonly IMapper _mapper;
            private readonly IQueryable<TEntity> _query;

            public Handler(IMapper mapper, IDbContext dbContext): base(mapper, dbContext)
            {
                _mapper = mapper;
                _query = Repo.Where(x => x.IsDeleted == false);
            }
        
            public override async Task<TOdto> Handle(TRequest request, CancellationToken cancellationToken)
            {
                var result = await _query
                    .Where( x => x.Id == request.Id)
                    .AsNoTracking()
                    .ProjectTo<TOdto>(_mapper.ConfigurationProvider)
                    .FirstOrDefaultAsync(cancellationToken);
                return result;
            }
        }
    }
}