﻿using System;
using System.ComponentModel.DataAnnotations;
using Entities.Models.Global;

namespace Web.UseCases.Dictionaries.BaseDictionaries.Dto
{
    public abstract class BaseDictionaryOdto
    {
        public Guid Id { get; set; }
        public string Code { get; set; }
        [Required] 
        public string NameRu { get; set; }
        public string NameKz { get; set; }
        public string NameEn { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
    }
}