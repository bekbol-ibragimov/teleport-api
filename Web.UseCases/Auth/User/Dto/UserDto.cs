﻿using System;
using Entities.Enums;
using Web.UseCases.Dictionaries.Companies.Dto;

namespace Web.UseCases.Auth.User.Dto
{
    public class UserDto
    {
        public Guid Id { get; set; }
        public string UserName { get; set; }
        public string Iin { get; set; }
        public string Email { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string FullName { get; set; }
        /// <summary>
        /// Дата рождения
        /// </summary>
        public DateTime? BirthDate { get; set; }
        /// <summary>
        /// Пол
        /// </summary>
        public GenderEnum? Sex { get; set; }
        
        /// <summary>
        /// Ссылка на фото
        /// </summary>
        public string PhotoPath { get; set; }

    }
}