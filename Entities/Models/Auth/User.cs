﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Entities.Enums;
using Entities.Models.Dictionaries;
using Entities.Models.Global;

namespace Entities.Models.Auth
{
    public class User: BaseEntity
    {
        public string PasswordHash { get; set; }
        [StringLength(50)]
        public string UserName { get; set; }
        [StringLength(200)]
        public string Iin { get; set; }
        [StringLength(200)]
        public string Email { get; set; }
        [StringLength(200)]
        public string LastName { get; set; }
        [StringLength(200)]
        public string FirstName { get; set; }
        [StringLength(200)]
        public string MiddleName { get; set; }
        public virtual string FullName => LastName + ' ' + FirstName + ' ' + MiddleName;
        /// <summary>
        /// Дата рождения
        /// </summary>
        public DateTime? BirthDate { get; set; }
        /// <summary>
        /// Пол
        /// </summary>
        public GenderEnum? Sex { get; set; }
        /// <summary>
        /// Ссылка на фото
        /// </summary>
        public string PhotoPath { get; set; }
        /// <summary>
        /// Актиный пользователь
        /// </summary>
        public bool IsActive { get; set; } = true;

        /// <summary>
        /// Участник кадрового резерва
        /// </summary>
        [Required]
        public bool IsEmployeePool { get; set; } = false;
        /// <summary>
        /// Если участник кадрового резерва то заполняется 
        /// </summary>
        public PoolEnum? Pool { get; set; }

        [ForeignKey(nameof(Company))]
        public Guid? CompanyId { get; set; }
        
        public Company Company { get; set; }
    }
}