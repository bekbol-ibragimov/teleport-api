﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using DataAccess.Interfaces;
using Entities.Models.Dictionaries;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Web.UseCases.Base;
using WebApp.Interfaces.Enums;

namespace Web.UseCases.Dictionaries.Localizations.Queries
{
    public class LocalizationGetTranslationsCommand : IRequest<Dictionary<string, string>>
    {
        public Lang Lang { get; set; }
        
        public class Handler : BaseDictionaryHandler<LocalizationGetTranslationsCommand,Dictionary<string, string>, Localization>
        {
            private readonly IQueryable<Localization> _query;
            public Handler(IMapper mapper, IDbContext dbContext): base(mapper, dbContext)
            {
                _query = Repo.Where(x => !x.IsDeleted);
            }

            public override async Task<Dictionary<string, string>> Handle(LocalizationGetTranslationsCommand request, CancellationToken cancellationToken)
            {
                var localizations = await _query.ToListAsync(cancellationToken);
                switch (request.Lang)
                {
                    case Lang.Ru:
                        return localizations.ToDictionary(localization => localization.Code, localization => localization.NameRu);
                    case Lang.Kz:
                        return localizations.ToDictionary(localization => localization.Code, localization => localization.NameKz);
                    case Lang.En:
                        return localizations.ToDictionary(localization => localization.Code, localization => localization.NameEn);
                    case Lang.Code:
                        return localizations.ToDictionary(localization => localization.Code, localization => localization.Code);
                    default:
                        return localizations.ToDictionary(localization => localization.Code, localization => localization.NameRu);
                }
            }
        }
    }
}