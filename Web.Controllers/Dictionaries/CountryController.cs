﻿using System;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Web.UseCases.Dictionaries.Countries.Commands.CreateCountry;
using Web.UseCases.Dictionaries.Countries.Dto;
using Web.UseCases.Dictionaries.Countries.Queries.GetById;

namespace Web.Controllers.Dictionaries
{
    public class CountryController: ControllerBase
    {
        private readonly ISender _sender;

        public CountryController(ISender sender)
        {
            _sender = sender;
        }
        
        [HttpPost]
        public async Task<Guid> Create([FromBody] CreateCountryDto dto)
        {
            var id = await _sender.Send(new CreateCountryCommand {Dto = dto});
            return id;
        }
        
        [HttpGet("{id:guid}")]
        public async Task<CountryDto> GetById(Guid id)
        {
            var result = await _sender.Send(new GetCountryByIdQuery() {Id = id});
            return result;
        }
        
    }
}