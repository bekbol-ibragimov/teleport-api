﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Newtonsoft.Json;

namespace Web.UseCases.Wrappers
{
    public class ApiResponse : ApiResponse<object>
    {
        public static ApiResponse Ok()
        {
            return new ApiResponse
            {
                Succeeded = true
            };
        }
        public new static ApiResponse Error(string error, string property = null)
        {
            return new ApiResponse()
            {
                Succeeded = false,
                Errors = new List<Error>()
                {
                    new Error()
                    {
                        Message = error,
                        Property = property
                    }
                }
            };
        }
    }
    public class ApiResponse<T>
    {
        public ApiResponse()
        {
        }

        public ApiResponse(T data)
        {
            Succeeded = true;
            Data = data;
        }

        public ApiResponse(string error)
        {
            Succeeded = false;
        }

        [JsonProperty("succeeded")] public bool Succeeded { get; set; }


        [JsonProperty("errors")] public List<Error> Errors { get; set; }

        [JsonProperty("data")] public T Data { get; set; }

        public static ApiResponse<T> Ok(T result)
        {
            return new ApiResponse<T>
            {
                Data = result,
                Succeeded = true
            };
        }
        
     

        public static ApiResponse<T> Error(System.Exception exception)
        {
            var msg = exception.Message;
            if (exception.InnerException != null) msg = exception.InnerException.Message;
            var res = new ApiResponse<T>
            {
                Succeeded = false,
                Errors = new List<Error>
                {
                    new Error()
                    {
                        Message = msg
                    }
                }
            };
            return res;
        }

        public static ApiResponse<T> Error(ModelStateDictionary modelState)
        {
            var res = new ApiResponse<T>()
            {
                Succeeded = false,
                Errors = new List<Error>()
            };
            res.Errors = modelState.Keys.SelectMany(key =>
            {
                return modelState[key].Errors.Select(e => new Error()
                {
                    Message = e.ErrorMessage,
                    Property = key
                });
            }).ToList();
            return res;
        }
        
        public static ApiResponse<T> Error(string error, string property = null)
        {
            return new ApiResponse<T>()
            {
                Succeeded = false,
                Errors = new List<Error>()
                {
                    new Error()
                    {
                        Message = error,
                        Property = property
                    }
                }
            };
        }
        
        public static ApiResponse<T> Error(List<Error> errors)
        {
            return new ApiResponse<T>()
            {
                Succeeded = false,
                Errors = errors
            };
        }

    }
}